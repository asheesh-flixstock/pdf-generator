const path = require('path');
const ClientProductModel = require('../config/database');
const {PDF_BUCKET_NAME} = require('../config/config');
const { downloadImagesForOutputSpec, downloadImagesForMannequin, downloadImagesForModelWithRetouching } = require('../services/downloadResourcesService');
const {
    getImageContentForOneLevelProperty,
    getImageContentForTwoLevelProperty,
    getTextContent,
    getTextContentInSameLine,
    addTextContentForSingleValue,
    getGoToDestinationContentForEachPage,
    getContentToBeAddedInEveryPage
} = require('../services/outputSpecificationContentService');
const {
    concatenateXAndY,
    getPropertyValue,
    getPropertyName,
    createImagesForContent,
    createBackgroundImageForContent    
} = require('../services/outputSpecificationContentService').helperMethods;
const { getContentForMannequin } = require('../services/mannequinContentService');
const { getContentForBrandDocument } = require('../services/brandContentService');
const { getContentForModel } = require('../services/modelContentService');
const { getContentForRetouching } = require('../services/retouchingContentService');
const { pdfkit } = require('../services/pdfKitService.js');
const { uploadFileToS3, removeFileFromS3UsingUrl } = require('../services/s3Service.js');
const { generateTenDigitUniqueString } = require('../services/uniqueStringService');

const imageOptions = { imageWidth: 58, imageHeight: 69, imageGapX: 16, imageTitleMarginTop: 8, imageTitleMarginBottom: 16, xMarginImages: 10 };
const imageOptionsForTwoImagesPerPage = { imageWidth: 244, imageHeight: 284, imageGapX: 16, imageTitleMarginTop: 8, imageTitleMarginBottom: 16, xMarginImages: 10 };
const imageOptionsForBrandDocuments = { imageWidth: 40, imageHeight: 50, imageGapX: 16, imageTitleMarginTop: 8, imageTitleMarginBottom: 16, xMarginImages: 10 };

const FILE_PATH = '/tmp';
const PDF_FILE_PATH = path.join(FILE_PATH);

const contentMapper = [
    {
        "dbKeyName": "outputSpecName",
        "keyName": "name",
        "displayTitle": "Name",
        "getContent": addTextContentForSingleValue,
        "path": ["outputSpec", "name"]
    },
    {
        "dbKeyName": "outputSpecCategory",
        "keyName": "categories",
        "displayTitle": "Categories",
        "getContent": getTextContentInSameLine(getPropertyName, ';'),
        "path": ["outputSpec", "targetGroup", "0", "properties", "categories"]
    },
    {
        "dbKeyName": "outputSpecAngle",
        "keyName": "angle",
        "displayTitle": "Angle",
        "getContent": getImageContentForTwoLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "angle"]
    },
    {
        "dbKeyName": "outputSpecDetails",
        "keyName": "detail",
        "displayTitle": "Detail Shots",
        "getContent": getImageContentForTwoLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "detail"]
    },
    {
        "dbKeyName": "outputSpecCropping",
        "keyName": "cropping",
        "displayTitle": "Cropping",
        "getContent": getImageContentForTwoLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "cropping"]
    },
    {
        "dbKeyName": "outputSpecOnFloorShadow",
        "keyName": "onFloorShadow",
        "displayTitle": "On Floor Shadow",
        "getContent": getImageContentForTwoLevelProperty(createImagesForContent(imageOptionsForTwoImagesPerPage), imageOptionsForTwoImagesPerPage),
        "path": ["outputSpec", "targetGroup", "0", "properties", "onFloorShadow"]
    },
    {
        "dbKeyName": "outputSpecProductShadow",
        "keyName": "productShadow",
        "displayTitle": "Product Shadow",
        "getContent": getImageContentForTwoLevelProperty(createImagesForContent(imageOptionsForTwoImagesPerPage), imageOptionsForTwoImagesPerPage),
        "path": ["outputSpec", "targetGroup", "0", "properties", "productShadow"]
    },
    {
        "dbKeyName": "outputSpecProductRetouching",
        "keyName": "productRetouching",
        "displayTitle": "Garment Retouching",
        "getContent": getImageContentForTwoLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "productRetouching"]
    },
    {
        "dbKeyName": "outputSpecWrinkle",
        "keyName": "wrinkle",
        "displayTitle": "Wrinkle",
        "getContent": getImageContentForTwoLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "wrinkle"]
    },
    // {
    //     "dbKeyName": "outputSpecProductRetouchingAndWrinkle",
    //     "keyName": "productRetouchingAndWrinkle",
    //     "displayTitle": "Garment Retouching & Wrinkle Removal",
    //     "getContent": getImageContentForTwoLevelProperty(createImagesForContent(imageOptionsForTwoImagesPerPage), imageOptionsForTwoImagesPerPage),
    //     "path": ["outputSpec", "_customFields", "productRetouchingAndWrinkle"]
    // },
    {
        "dbKeyName": "outputSpecBackground",
        "keyName": "background",
        "displayTitle": "Background",
        "getContent": getImageContentForTwoLevelProperty(createBackgroundImageForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "background"]
    },
    {
        "dbKeyName": "outputSpecResolution",
        "keyName": "resolution",
        "displayTitle": "Resolution",
        "getContent": getTextContent(concatenateXAndY),
        "path": ["outputSpec", "targetGroup", "0", "properties", "resolution"]
    },
    {
        "dbKeyName": "outputSpecDpi",
        "keyName": "dpi",
        "displayTitle": "DPI",
        "getContent": getTextContent(getPropertyValue),
        "path": ["outputSpec", "targetGroup", "0", "properties", "dpi"]
    },
    {
        "dbKeyName": "outputSpecOutputFileFormat",
        "keyName": "outputFileFormat",
        "displayTitle": "File Formats",
        "getContent": getTextContent(getPropertyName),
        "path": ["outputSpec", "targetGroup", "0", "properties", "outputFileFormat"]
    },
    {
        "dbKeyName": "outputSpecOnFloorShadowAll",
        "keyName": "onFloorShadow_all",
        "displayTitle": "On Floor Shadow",
        "getContent": getImageContentForOneLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "onFloorShadow_all"]
    },
    {
        "dbKeyName": "outputSpecProductShadowAll",
        "keyName": "productShadow_all",
        "displayTitle": "Garment Shadow",
        "getContent": getImageContentForOneLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "productShadow_all"]
    },
    {
        "dbKeyName": "outputSpecProductRetouchingAndWrinkle",
        "keyName": "productRetouchingAndWrinkle_all",
        "displayTitle": "Garment Retouching & Wrinkle Removal",
        "getContent": getImageContentForOneLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "productRetouchingAndWrinkle_all"]
    },
    {
        "dbKeyName": "outputSpecLightingAll",
        "keyName": "lighting_all",
        "displayTitle": "Lighting",
        "getContent": getImageContentForOneLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "lighting_all"]
    },
    {
        "dbKeyName": "outputSpecBackgroundAll",
        "keyName": "background_all",
        "displayTitle": "Background",
        "getContent": getImageContentForOneLevelProperty(createImagesForContent(imageOptions), imageOptions),
        "path": ["outputSpec", "targetGroup", "0", "properties", "background_all"]
    },
    // {
    //     "keyName": "lighting",
    //     "displayTitle": "Lighting",
    //     "getContent": getImageContentForTwoLevelProperty(createImagesForContent(imageOptions), imageOptions),
    //     "path": ["outputSpec", "targetGroup", "0", "properties", "lighting"]
    // },
    {
        "dbKeyName": "mannequin",
        "keyName": "mannequin",
        "displayTitle": "Mannequin",
        "getContent": getContentForMannequin(imageOptions),
        "path": ["mannequin"]
    },
    {
        "dbKeyName": "aboutTheBrand",
        "keyName": "aboutTheBrand",
        "displayTitle": "About the Brand",
        "getContent": getContentForBrandDocument(imageOptionsForBrandDocuments),
        "path": ["aboutTheBrand"]
    },
    {
        "dbKeyName": "model",
        "keyName": "modelWithRetouching",
        "displayTitle": "Models",
        "getContent": getContentForModel(imageOptions),
        "path": ["modelWithRetouching"]
    },
     {
        "dbKeyName": "retouching",
        "keyName": "modelWithRetouching",
        "displayTitle": "Retouching",
        "getContent": getContentForRetouching(imageOptions),
        "path": ["modelWithRetouching"]
    },
]

exports.generateGenericPdf = generateGenericPdf;

async function generateGenericPdf(type, projectId, clientProductId, outputSpecId) {
    console.log(`Request information --> type = ${type}, projectId = ${projectId}, clientProductId=${clientProductId}, outputSpecId=${outputSpecId} `);
    
    let contentMapper = await getContentWrapperBasedOnType(type);
    console.log("Content mapper --> ", JSON.stringify(contentMapper))
    
    let sourcesList = createSourceList(contentMapper);
    console.log("Source List --> ", JSON.stringify(sourcesList))
    
    let sources = await createSourceObject(sourcesList, projectId, clientProductId, outputSpecId);
    console.log("Source Details --> ", JSON.stringify(sources))
    
    console.log('Downloading the images')
    await downloadImagesForAllSources(sources, contentMapper);    
    
    let content = await createContent(contentMapper, sources, type)
    console.log("PDF Content , ", JSON.stringify(content));

    let pdfUrl = await createAndSavePdf(content, sources, projectId, clientProductId, outputSpecId, type);
    
    return pdfUrl;
}

function createSourceList(contentMapper) {
    let sourcesList = new Set();
    for (let el of contentMapper) {
        if (!(el && el.path && el.path[0])) continue;
        sourcesList.add(el.path[0]);
    }
    return sourcesList
}


async function getContentWrapperBasedOnType(type) {
    let pdfContent =  await ClientProductModel.getPdfContentByType(type);
    if (!pdfContent) return [];
    let contentMapperCustom = pdfContent.map(dbKeyName => {
        for (let contMapper of contentMapper) {
            if (contMapper.dbKeyName == dbKeyName) return contMapper;
        }
    });
    return contentMapperCustom.filter(el => el);
}

async function createSourceObject(sourcesList, projectId, clientProductId, outputSpecId) {
    let sources = {};
    for (let source of sourcesList) {
        sources[source] = await getSource(source, projectId, clientProductId, outputSpecId);
    }

    // Filtering mannequin data based on target group of output specification
    if (sources['mannequin'] && sources['outputSpec']) {
        let targetGroupName = extractSourceValueFromPath(sources, ["outputSpec", "targetGroup", "0", "name"]); 
        let mannequinDataForTargetGroup = sources['mannequin'] && sources['mannequin'][0] && sources['mannequin'][0].mannequin && sources['mannequin'][0].mannequin[targetGroupName];
        let mannequinId = sources['mannequin'][0] && sources['mannequin'][0]._id;
        if (!mannequinDataForTargetGroup) mannequinDataForTargetGroup = [];
        sources['mannequin'] = [{ _id: mannequinId, mannequin: {[targetGroupName]: mannequinDataForTargetGroup} }];
    }

    if (sources['modelWithRetouching'] && sources['outputSpec']) {
        let targetGroupId = extractSourceValueFromPath(sources, ["outputSpec", "targetGroup", "0", "_id"]); 
        sources['modelWithRetouching'] = await ClientProductModel.getModelsWithRetouching(clientProductId, targetGroupId)
    }
    
    return sources;
}

function getSource(source, projectId, clientProductId, outputSpecId) {
    switch(source) {
        case 'outputSpec': 
            return ClientProductModel.getSelectedOutputSpecification(clientProductId, outputSpecId);
        case 'mannequin': 
            return ClientProductModel.getAllSelectedMannequins(clientProductId);
        case 'aboutTheBrand': 
            return ClientProductModel.getBrandDocument(projectId);
        case 'modelWithRetouching':
            return {} // Data will be fetched based on target group id fetched from output spec
    }
}

function downloadImagesForAllSources(sources, contentMapper) {
    let promiseArr = [];
    if (sources['outputSpec']) {
        let outputSpecFieldsForDownload = contentMapper.filter(el => el && el.path && el.path[0] == "outputSpec").map(el => el.keyName);
        promiseArr.push(downloadImagesForOutputSpec(sources['outputSpec'], outputSpecFieldsForDownload));
    }
    if (sources['mannequin']) {
        promiseArr.push(downloadImagesForMannequin(sources['mannequin']));
    }
    if (sources['modelWithRetouching']) {
        promiseArr.push(downloadImagesForModelWithRetouching(sources['modelWithRetouching']));
    }
    return Promise.all(promiseArr)
        .catch(er => console.log("Error downloading the images", er));
}

async function createContent(contentMapper, sources, type) {
    let content = [];
    let destinations = [];
    
    for (let contentInfo of contentMapper) {
        let value = extractSourceValueFromPath(sources, contentInfo.path);
        // if (!value) continue;
        destinations.push(contentInfo.displayTitle);
        let contentToBeAdded = await contentInfo.getContent(value);
        content.push(contentToBeAdded);
    }

    let titleName = getTitleNameByType(type, sources);
    content.push(getContentToBeAddedInEveryPage(titleName, destinations));

    return content;
}

async function createAndSavePdf(content, sources, projectId, clientProductId, outputSpecId, type) {
    let clientName = await ClientProductModel.getClientName(clientProductId);
    let outputSpecName = extractSourceValueFromPath(sources, ["outputSpec", "name"]);
    let fileName = getFileNameByType(clientName, outputSpecName, type);
    let filePath = path.join(PDF_FILE_PATH, fileName);
    await pdfkit(filePath, { content, contentType: 'content' });
    let pdfUrl = await uploadFileToS3(filePath, 'outputSpec/' + fileName, PDF_BUCKET_NAME);
    let oldPdfDoc = await ClientProductModel.savePdfUrl(type, pdfUrl, projectId, clientProductId, outputSpecId);
    let oldPdfUrl = oldPdfDoc && oldPdfDoc.value && oldPdfDoc.value.pdfUrl;
    if (oldPdfUrl != pdfUrl) await removeFileFromS3UsingUrl(oldPdfUrl, PDF_BUCKET_NAME);
    return pdfUrl;
}

function extractSourceValueFromPath(source, path) {
    let value = source;
    for (let key of path) {
        if (!value) return;
        value = value[key];
    }
    return value;
}

function getFileNameByType(clientName, outputSpecName, type) {
    // ClientName, OutputName, PdfType, RandomString
    switch(type) {
        case 'generic': 
            return `${clientName}_${outputSpecName}_${generateTenDigitUniqueString()}_Generic.pdf`;
        case 'garmentRetouching':
            return `${clientName}_${outputSpecName}_${generateTenDigitUniqueString()}_GarmentRetouching.pdf`;
        case 'draping':
            return `${clientName}_${outputSpecName}_${generateTenDigitUniqueString()}_Draping.pdf`;
        case 'template':
            return `${clientName}_${outputSpecName}_${generateTenDigitUniqueString()}_Template.pdf`;
        case 'outputSpecification':
            return `${clientName}_${outputSpecName}_${generateTenDigitUniqueString()}.pdf`;
        case 'mannequin':
            return `${clientName}_Mannequin_${generateTenDigitUniqueString()}.pdf`;
    }
}

function getTitleNameByType(type, sources) {
    let targetGroupName = extractSourceValueFromPath(sources, ["outputSpec", "targetGroup", "0", "name"]);
    if (!targetGroupName) targetGroupName = '';
    targetGroupName = targetGroupName.charAt(0).toUpperCase() + targetGroupName.slice(1);

    switch(type) {
        case 'draping':
            return targetGroupName ? `Draping: ${targetGroupName}`: `Draping`;
        case 'template':
            return targetGroupName ? `Template: ${targetGroupName}`: `Template`;
        case 'generic':
            return targetGroupName ? `Generic: ${targetGroupName}`: `Generic`;
        case 'garmentRetouching':
            return targetGroupName ? `Garment Retouching: ${targetGroupName}`: `Garment Retouching`;
        case 'outputSpecification':
            return targetGroupName ? `Output Specification: ${targetGroupName}`: `Output Specification`;
        case 'mannequin':
            return 'Mannequin';
    }
}