const path = require('path');
const ClientProductModel = require('../config/database.js');
const { downloadImage } = require('../services/downloadImageService.js');
const { createTextContent, createImagesContent } = require('../services/helpers/contentHelper.js');
const { pdfkit } = require('../services/pdfKitService.js');
const { uploadFileToS3 } = require('../services/s3Service.js'); 
const FILE_PATH = '/tmp';
const IMAGE_FILE_PATH = path.join(FILE_PATH);
const PDF_FILE_PATH = path.join(FILE_PATH);

const imageKeys = [
    { "keyName": 'mannequinFrontInfo', 'imageTitle': 'Front' },
    { "keyName": 'mannequinBackInfo', 'imageTitle': 'Back' },
    { "keyName": 'mannequinSideInfo', 'imageTitle': 'Side' }
];

exports.generatePdfForMannequin = generatePdfForMannequin;

async function generatePdfForMannequin(clientProductId) {
    let mannequinData = await ClientProductModel.getAllSelectedMannequins(clientProductId);
    if (!(mannequinData && mannequinData[0] && mannequinData[0].mannequin)) return;
    mannequinData = mannequinData[0].mannequin;
    await downloadImagesForMannequin(mannequinData);
    let contentDetails = { contentType: 'content', content: [] };
    for (let targetGroupName of Object.keys(mannequinData)) addMannequinDetailsIntoContent(mannequinData, contentDetails, targetGroupName);
    let fileName = `Mannequin_${clientProductId}.pdf`;
    let filePath = path.join(PDF_FILE_PATH, fileName);
    await pdfkit(filePath, contentDetails);
    let pdfUrl = await uploadFileToS3(filePath, 'mannequin/' + fileName, process.env.PDF_BUCKET_NAME);
    return pdfUrl;
}

async function downloadImagesForMannequin(mannequinData) {
    let arr = [];
    for (let targetGroupName of Object.keys(mannequinData)) {
        for (let mann of mannequinData[targetGroupName]) {
            for (let key of imageKeys) {
                if (!mann.mannequinData[key.keyName]) continue;
                let fileUrl = mann.mannequinData[key.keyName].fileUrl;
                let filePath = getFilePath(mann.mannequinData[key.keyName]);
                arr.push(downloadImage(fileUrl, filePath));
            }
        }
    }
    return Promise.all(arr);
}

function addMannequinDetailsIntoContent(mannequinData, contentDetails, targetGroupName) {
    if (!(mannequinData && mannequinData[targetGroupName])) return;
    contentDetails.content.push(createTextContent(capitalizeString(targetGroupName), 12));
    let types = getUniqueTypes(mannequinData[targetGroupName]);
    for (let type of types) addMannequinTypeIntoContent(mannequinData, contentDetails, targetGroupName, type);
}

function addMannequinTypeIntoContent(mannequinData, contentDetails, targetGroupName, type) {
    if (!(mannequinData && mannequinData[targetGroupName])) return;
    let mannequinByType = mannequinData[targetGroupName].filter(mann => mann.type == type);
    if (!mannequinByType.length) return;
    contentDetails.content.push(createTextContent(capitalizeString(type), 10));
    for (let mann of mannequinByType) {
        contentDetails.content.push(createTextContent(mann.mannequinName, 8));
        contentDetails.content.push(createImagesContent(extractMannequinImages(mann), 75, 90));
    }
}

function capitalizeString(str) {
    if (!str) return;
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function getUniqueTypes(mannequinTargetGroup) {
    if (!mannequinTargetGroup) return [];
    let types = {};
    for (let mann of mannequinTargetGroup) types[mann.type] = true;
    return Object.keys(types);
}

function extractMannequinImages(mannequin) {
    if (!(mannequin && mannequin.mannequinData)) return;
    let images = [];
    
    for (let key of imageKeys) {
        if (!mannequin.mannequinData[key.keyName]) continue;
        images.push(convertPropertyToPdfContent(mannequin.mannequinData[key.keyName], key.imageTitle));
    }
    return images;
}

function convertPropertyToPdfContent(property, imageTitle) {
    return {
        'path': getFilePath(property),
        imageTitle,
        'fontSize': 8,
        'skip': true
    };
}

function getFilePath(property) {
    return path.join(IMAGE_FILE_PATH, extractFileName(property));
}

function extractFileName(property) {
    return property.fileId + '.' + extractImageFormatFromUrl(property.fileUrl);
}

function extractImageFormatFromUrl(fileUrl) {
    if (!fileUrl) return;
    let dotSplitImage = fileUrl.split('.');
    return dotSplitImage[dotSplitImage.length - 1];
}
