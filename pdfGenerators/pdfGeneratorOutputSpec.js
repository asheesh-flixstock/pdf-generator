const path = require('path');
const ClientProductModel = require('../config/database.js');
const { downloadImage } = require('../services/downloadImageService.js');
const { createTextContent, createImagesContent, createBackgroundImagesContent, addPageBreak, addDestination, addGoToDestination, addContentsInEachPage, addContentForVerticalSpace } = require('../services/helpers/contentHelper.js');
const { pdfkit } = require('../services/pdfKitService.js'); 
const { uploadFileToS3 } = require('../services/s3Service.js'); 
const FILE_PATH = '/tmp';
const IMAGE_FILE_PATH = path.join(FILE_PATH);
const PDF_FILE_PATH = path.join(FILE_PATH);
const HEADING_FONT = 'Roboto-Bold';
const SUB_HEADING_FONT = 'Roboto-Bold';
const TEXT_FONT = 'Roboto-Light';

const targetGroupFields = [
  {
    "keyName": "angle",
    "displayTitle": "Angle",
    "getContent": getImageContentForTwoLevelProperty(createImagesForContent(77, 90))
  },
  {
    "keyName": "detail",
    "displayTitle": "Detail",
    "getContent": getImageContentForTwoLevelProperty(createImagesForContent(77, 90))
  },
  {
    "keyName": "cropping",
    "displayTitle": "Cropping",
    "getContent": getImageContentForTwoLevelProperty(createImagesForContent(77, 90))
  },
  {
    "keyName": "onFloorShadow",
    "displayTitle": "On Floor Shadow",
    "getContent": getImageContentForTwoLevelProperty(createImagesForContent(77, 90))
  },
  {
    "keyName": "productShadow",
    "displayTitle": "Product Shadow",
    "getContent": getImageContentForTwoLevelProperty(createImagesForContent(77, 90))
  },
  {
    "keyName": "productRetouching",
    "displayTitle": "Product Retouching",
    "getContent": getImageContentForTwoLevelProperty(createImagesForContent(77, 90))
  },
  {
    "keyName": "wrinkle",
    "displayTitle": "Wrinkle",
    "getContent": getImageContentForTwoLevelProperty(createImagesForContent(77, 90))
  },
  {
    "keyName": "background",
    "displayTitle": "Background",
    "getContent": getImageContentForTwoLevelProperty(createBackgroundImageForContent(77, 90))
  },
  {
    "keyName": "resolution",
    "displayTitle": "Resolution",
    "getContent": getTextContent(concatenateXAndY)
  },
  {
    "keyName": "dpi",
    "displayTitle": "DPI",
    "getContent": getTextContent(getPropertyValue)
  },
  {
    "keyName": "outputFileFormat",
    "displayTitle": "Output File Format",
    "getContent": getTextContent(getPropertyName)
  },
  {
    "keyName": "categories",
    "displayTitle": "Categories",
    "getContent": getTextContent(getPropertyName)
  },
  // {
  //   "keyName": "namingConvention",
  //   "displayTitle": "Naming Convention",
  //   "getContent": getTextContent(getPropertyName)
  // },
  // {   
  //  "keyName": "namingConventionCustom",
  //   "displayTitle": "Custom Naming convention",
  //   "getContent": getImageContentForTwoLevelProperty(createImagesForContent(77, 90))
  // }
];

exports.generatePDFForOutputSpec = generatePDFForOutputSpec;

async function generatePDFForOutputSpec(clientProductId, outputSpecId) {
    let outputSpec = await ClientProductModel.getSelectedOutputSpecification(clientProductId, outputSpecId);
    let outputSpecProperties = outputSpec && outputSpec.targetGroup && outputSpec.targetGroup[0] && outputSpec.targetGroup[0].properties;
    if (!outputSpecProperties) return;
    await downloadImagesForOutputSpec(outputSpecProperties);

    let content = [];
    let destinations = [];
    for (let prop of targetGroupFields) {
      if (!outputSpecProperties[prop.keyName]) continue;
      destinations.push(prop.displayTitle);
      content.push(prop.getContent(outputSpecProperties[prop.keyName]));
    }
    
    content.push(addContentsInEachPage([addGoToDestination(destinations)]))

    let fileName = `Output_Spec_${outputSpecId}.pdf`;
    let filePath = path.join(PDF_FILE_PATH, fileName); 
    await pdfkit(filePath, {content, contentType: 'content'});
    let pdfUrl = await uploadFileToS3(filePath, 'outputSpec/' + fileName, process.env.PDF_BUCKET_NAME);
    return pdfUrl;
}

function downloadImagesForOutputSpec(outputSpecProperties) {
    let imageUrlAndIdList = extractImageIdAndUrl(outputSpecProperties);
    let downlaodPromiseArray = [];
    for (let image of imageUrlAndIdList) {
        downlaodPromiseArray.push(downloadImage(...image));
    }
    return Promise.all(downlaodPromiseArray);
}

function extractImageIdAndUrl(outputSpecProperties) {
    let imageUrlAndIdList = [];
    for (let field of targetGroupFields) {
        let stack = [];
        field = field.keyName;
        stack.push({children: outputSpecProperties[field]});
        while (stack.length) {
            let {children} = stack.pop();
            if (!children) continue;
            let grandChildren = Array.isArray(children) ? children : children.values; 
            
            if (!grandChildren || !(Array.isArray(grandChildren))) {
                // leaf node
                if (!children.image) continue;
                imageUrlAndIdList.push([children.image.fileUrl, getFilePath(children)]);
                continue;
            }

            if (!grandChildren) continue;
            for (let child of grandChildren) {
                stack.push({ children: child });
            }
        }        
    }
    return imageUrlAndIdList;
}

function getImageContentForTwoLevelProperty(leafContentCallback) {
  return function(outputSpecProperty) {
    let contentDetails = { content: [], contentType: 'content' };
    //adding destination
    contentDetails.content.push(addDestination(this.displayTitle));
    // adding title
    contentDetails.content.push(createTextContent(this.displayTitle, 14, HEADING_FONT));
    // adding margin
    contentDetails.content.push(addContentForVerticalSpace(10));

    // adding the rest of the content
    for (let prop of outputSpecProperty) {
      // adding title
      contentDetails.content.push(createTextContent(prop.category, 10, SUB_HEADING_FONT));
      contentDetails.content.push(addContentForVerticalSpace(8));

      if (prop.values) {
        // adding the rest of the content
        let imagesContent = prop.values.map(convertPropertySchemaToPdfContent);
        contentDetails.content.push(leafContentCallback(imagesContent));
      }
      if (prop.additionalValues) pushAdditionalValuesInContent(prop.additionalValues, contentDetails);

      contentDetails.content.push(addContentForVerticalSpace(2));
    }
    // page break
    contentDetails.content.push(addPageBreak());

    return contentDetails; 
  }
}

function getTextContent(getText) {
  return function(outputSpecProperty) {
    let contentDetails = { content: [], contentType: 'content' };
    //adding destination
    contentDetails.content.push(addDestination(this.displayTitle));
    //adding title
    contentDetails.content.push(createTextContent(this.displayTitle, 14, HEADING_FONT));
    // adding space after title
    contentDetails.content.push(addContentForVerticalSpace(8));

    for (let prop of outputSpecProperty)  {
      let text = getText(prop);
      contentDetails.content.push(createTextContent(text, 12, TEXT_FONT));
    }

    contentDetails.content.push(addContentForVerticalSpace(16));
    return contentDetails;  
  }
}

function concatenateXAndY(prop) {
  return `${prop.x} x ${prop.y}`;
}

function getPropertyValue(prop) {
  return `${prop.value}`;
}

function getPropertyName(prop) {
  return prop.name;
}


function createImagesForContent(imageWidth, imageHeight) {
  return function(content) {
    return createImagesContent(content, imageWidth, imageHeight)
  }
}

function createBackgroundImageForContent(rectWidth, rectHeight) {
  return function(content) {
    return createBackgroundImagesContent(content, rectWidth, rectHeight);
  }
}

function pushAdditionalValuesInContent(additionalValues, contentDetails) {
  for (let addKeys of Object.keys(additionalValues)) {
    contentDetails.content.push(createTextContent(capitalizeString(addKeys), 10, SUB_HEADING_FONT));
    contentDetails.content.push(addContentForVerticalSpace(8));

    let text = ``;
    for (let property of additionalValues[addKeys]) {
      if (text !== '') text += ', ';
      text += `${property.name}`;
      if (property.value) text += `: ${property.value}`;      
    }
    contentDetails.content.push(createTextContent(text, 8, TEXT_FONT));
    contentDetails.content.push(addContentForVerticalSpace(16));
  }
}

function convertPropertySchemaToPdfContent(property) {
  if (!property) return;
  return {
    'path': getFilePath(property),
    'imageTitle': property && property.name,
    'fontSize': 7,
    'skip': true,
    'font': TEXT_FONT
  };
}

function getFilePath(property) {
  return path.join(IMAGE_FILE_PATH, extractFileName(property));
}

function extractFileName(property) {
  if (!(property && property.image)) return '';
  return property.image.fileId + '.' + extractImageFormatFromUrl(property.image.fileUrl);
}

function extractImageFormatFromUrl(fileUrl) {
  if (!fileUrl) return;
  let dotSplitImage = fileUrl.split('.');
  return dotSplitImage[dotSplitImage.length-1];
}

function capitalizeString(str) {
  if (!str) return;
  return str.charAt(0).toUpperCase() + str.slice(1);
}