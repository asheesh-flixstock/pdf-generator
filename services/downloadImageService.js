const fs = require('fs');
const https = require('https');

exports.downloadImage = downloadImage;

function downloadImage(fileUrl, filePath) {
    try {
        if (filePath == '/tmp') return Promise.resolve();
        fileUrl = fileUrl.replace(/^http:/, 'https:');
        return new Promise((resolve, reject) => {
            https.get(fileUrl, function(response) {
                if (response.statusCode != 200) {
                    resolve();
                }
                const fileWriteStream = fs.createWriteStream(filePath);
                response.pipe(fileWriteStream).on('close', () => {
                    resolve();
                });
            });
        })    
    } catch(err) {
        console.log(`Error in downloading file from ${fileUrl} !! -> `, JSON.stringify(err));
        return Promise.resolve();
    }
};