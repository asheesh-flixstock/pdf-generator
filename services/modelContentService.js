const { createTextContent, createImagesContent, createImageOnRightContent, createBackgroundImagesContent, addPageBreak, addDestination, addGoToDestination, addContentsInEachPage, addContentForVerticalSpace, addHeadingTitleContent, addContentForConditionalPageBreak, addContentForNotAvailableResources } = require('../services/helpers/contentHelper.js');
const { getHeightForText } = require('./pdfKitService');
const { getFilePath } = require('./filePathService');
const { getImageHeightBasedOnWidth } = require('./imageSizeService');
const PAGE_WIDTH = 594;
const X_MARGIN_LEFT = 118;
const X_MARGIN_RIGHT = 10;
const TITLE_SIZE = 14;
const TITLE_MARGIN_BOTTOM = 10;
const MODEL_TYPE_SIZE = 12;
const MODEL_TYPE_MARGIN_BOTTOM = 8;
const MODEL_TEXT_SIZE = 10;
const MODEL_TEXT_MARGIN_BOTTOM = 5;
const IMAGE_TITLE_FONT_SIZE = 8;
const HEADING_FONT = 'Roboto-Bold';
const TEXT_FONT = 'Roboto-Light';

exports.getContentForModel = getContentForModel;

function getContentForModel(options) {
    return async function(modelListByType) {
        let contentDetails = { contentType: 'content', content: [] };
        if (!modelListByType) return contentDetails;
        let keys = Object.keys(modelListByType).sort();

        let modelExists = false;
        for (let key of keys) {
            let modelsByType = modelListByType[key];
            for (let model of modelsByType) {
                if (model.models && model.models.length) {
                    modelExists = true;
                    break;
                }
            }
        }

        if (!modelExists) {
            let heightToBeWrapped = 
                TITLE_SIZE + TITLE_MARGIN_BOTTOM +
                getHeightForText('N/A', { fontSize: 10});
            contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));
        
            contentDetails.content.push(addDestination(this.displayTitle));
            
            contentDetails.content.push(createTextContent(this.displayTitle, TITLE_SIZE, HEADING_FONT));
            contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));
            contentDetails.content.push(addContentForNotAvailableResources());
            contentDetails.content.push(addContentForVerticalSpace(16));
            return contentDetails;
        }


        let firstModelList = modelListByType && modelListByType[keys[0]] ? extractAllModelListAndSortByName(modelListByType[keys[0]]) : [];
        let heightToBeWrapped = 
            TITLE_SIZE + TITLE_MARGIN_BOTTOM +
            MODEL_TYPE_SIZE + MODEL_TYPE_MARGIN_BOTTOM + 
            getHeightForFirstRowOfModels(firstModelList, options);
        contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));
    
        contentDetails.content.push(addDestination(this.displayTitle));
        
        contentDetails.content.push(createTextContent(this.displayTitle, TITLE_SIZE, HEADING_FONT));
        contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));
    
        await addContentForModelsOfAllTypes(modelListByType, options, contentDetails);
        
        console.log("Model content --> ", JSON.stringify(contentDetails));
        
        return contentDetails;
    }
}

function getHeightForFirstRowOfModels(modelList, options) {
    // let modelList = modelListByType[Object.keys(modelListByType)[0]];
    let { imageHeight, imageWidth, imageGapX, imageTitleMarginTop, imageTitleMarginBottom, xMarginImages } = options;
    let cols = Math.floor((PAGE_WIDTH - (X_MARGIN_LEFT + xMarginImages + X_MARGIN_RIGHT))/(imageWidth + imageGapX));
    let heightForContent;

    // Finding out the longest name
    let longestImageTitleInFirstRow = '';
    for (let i=0; i<cols; i++) {
        if (i >= modelList.length) break;
        if (!(modelList[i] && modelList[i].modelName)) continue;
        if (modelList[i].modelName.length > longestImageTitleInFirstRow) {
            longestImageTitleInFirstRow = modelList[i].modelName;
        }
    }    

    heightForContent = getHeightForText(longestImageTitleInFirstRow, {width: imageWidth, fontSize: IMAGE_TITLE_FONT_SIZE});
    return imageHeight + imageTitleMarginTop + imageTitleMarginBottom + heightForContent;
}


async function addContentForModelsOfAllTypes(modelListByType, options, contentDetails) {
    let keys = Object.keys(modelListByType).sort();
    for (let modelByTypeKey of keys) {
        let modelByType = modelListByType[modelByTypeKey];
        let modelList = extractAllModelListAndSortByName(modelByType);
        if (!(modelList && modelList.length)) continue;

        let heightToBeWrappedForType = 
            MODEL_TYPE_SIZE + MODEL_TYPE_MARGIN_BOTTOM + 
            getHeightForFirstRowOfModels(modelList, options);
        contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrappedForType));

        let modelTypeWithCapitalCase = modelByTypeKey.charAt(0).toUpperCase() + modelByTypeKey.slice(1);
        contentDetails.content.push(createTextContent(modelTypeWithCapitalCase, MODEL_TYPE_SIZE, HEADING_FONT));
        contentDetails.content.push(addContentForVerticalSpace(MODEL_TYPE_MARGIN_BOTTOM));

        contentDetails.content.push(createImagesContent(modelList.map(convertModelIntoPdfContent), options));
        contentDetails.content.push(addContentForVerticalSpace(16));
        
    }
}

function extractAllModelListAndSortByName(modelByType) {
    let modelList = [];
    for (let modelDetails of modelByType) {
        if (!(modelDetails && modelDetails.models)) continue;
        modelList.push(...modelDetails.models)
    }
    return modelList.sort((m1, m2) => m1.modelName < m2.modelName ? -1 : 1 );
}

function convertModelIntoPdfContent(modelDetails) {
    if (!modelDetails) return;
    return {
        'path': getFilePath(modelDetails.image),
        'imageTitle': modelDetails.modelName,
        'fontSize': IMAGE_TITLE_FONT_SIZE,
        'skip': true,
        'font': TEXT_FONT
    };
}

