const { downloadImage } = require('./downloadImageService.js');
const imageKeys = [
    { "keyName": 'mannequinFrontInfo', 'imageTitle': 'Front' },
    { "keyName": 'mannequinBackInfo', 'imageTitle': 'Back' },
    { "keyName": 'mannequinSideInfo', 'imageTitle': 'Side' }
];
const {getFilePath} = require('./filePathService');

exports.downloadImagesForOutputSpec = downloadImagesForOutputSpec;
exports.downloadImagesForMannequin = downloadImagesForMannequin;
exports.downloadImagesForModelWithRetouching = downloadImagesForModelWithRetouching;

function downloadImagesForOutputSpec(outputSpec, fields) {
    let outputSpecProperties = outputSpec && outputSpec.targetGroup && outputSpec.targetGroup[0] && outputSpec.targetGroup[0].properties;
    let imageUrlAndIdList = extractImageIdAndUrlForOutputSpec(outputSpecProperties, fields);
    let downlaodPromiseArray = [];
    for (let image of imageUrlAndIdList) {
        downlaodPromiseArray.push(downloadImage(...image));
    }
    return Promise.all(downlaodPromiseArray);
}

async function downloadImagesForMannequin(mannequinData) {
    mannequinData = mannequinData && mannequinData[0] && mannequinData[0].mannequin;
    if (!mannequinData) return Promise.resolve();
    let arr = [];
    for (let targetGroupName of Object.keys(mannequinData)) {
        for (let mann of mannequinData[targetGroupName]) {
            for (let key of imageKeys) {
                if (!mann.mannequinData[key.keyName]) continue;
                let fileUrl = mann.mannequinData[key.keyName].fileUrl;
                let filePath = getFilePath(mann.mannequinData[key.keyName]);
                arr.push(downloadImage(fileUrl, filePath));
            }
        }
    }
    return Promise.all(arr);
}

function downloadImagesForModelWithRetouching(modelDetails) {
    let arr = [];
    for (let modelType of Object.keys(modelDetails)) {
        let modelListByType = modelDetails[modelType];
        for (let modelByType of modelListByType) {
            if (modelByType && modelByType.retouching && modelByType.retouching.image) {
                let retouchingFileUrl = modelByType.retouching.image.fileUrl;
                let retouchingFilePath = getFilePath(modelByType.retouching.image);
                // console.log('Retouchign file path and url --> ', retouchingFileUrl, retouchingFilePath);
                arr.push(downloadImage(retouchingFileUrl, retouchingFilePath));                    
            }
            let modelListByRetouching = modelByType.models;
            for (let modelByRetouching of modelListByRetouching) {
                if (modelByRetouching && modelByRetouching.image) {
                    let modelFileUrl = modelByRetouching.image.fileUrl;
                    let modelFilePath = getFilePath(modelByRetouching.image);
                    arr.push(downloadImage(modelFileUrl, modelFilePath)); 
                    // console.log('Model file path and url --> ', modelFileUrl, modelFilePath);
                }
            }
        }
    }
    return Promise.all(arr);
}

function extractImageIdAndUrlForOutputSpec(outputSpecProperties, fields) {
    if (!outputSpecProperties) return [];
    let imageUrlAndIdList = [];
    for (let field of fields) {
        let stack = [];
        stack.push({ children: outputSpecProperties[field] });
        while (stack.length) {
            let { children } = stack.pop();
            if (!children) continue;
            let grandChildren = Array.isArray(children) ? children : children.values;

            if (!grandChildren || !(Array.isArray(grandChildren))) {
                // leaf node
                if (!children.image) continue;
                imageUrlAndIdList.push([children.image.fileUrl, getFilePath(children)]);
                continue;
            }

            if (!grandChildren) continue;
            for (let child of grandChildren) {
                stack.push({ children: child });
            }
        }
    }
    return imageUrlAndIdList;
}

