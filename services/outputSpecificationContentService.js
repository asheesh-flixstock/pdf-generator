const { createTextContent, createImagesContent, createBackgroundImagesContent, addPageBreak, addDestination, addGoToDestination, addContentsInEachPage, addContentForVerticalSpace, addHeadingTitleContent, addContentForConditionalPageBreak, addContentForNotAvailableResources } = require('../services/helpers/contentHelper.js');
const { getFilePath } = require('./filePathService');
const { getHeightForText } = require('./pdfKitService');
const HEADING_FONT = 'Roboto-Bold';
const SUB_HEADING_FONT = 'Roboto-Bold';
const TEXT_FONT = 'Roboto-Light';
const IMAGE_TITLE_FONT_SIZE = 7;
const PAGE_WIDTH = 594;
const X_MARGIN_LEFT = 118;
const X_MARGIN_RIGHT = 10;
const TITLE_SIZE = 14;
const TITLE_MARGIN_BOTTOM = 10;
const CONTENT_SIZE = 12;
const SUB_TITLE_SIZE = 10;
const SUB_TITLE_MARGIN_BOTTOM = 8; 
const ADDITIONAL_VALUES_TITLE_SIZE = 8;
const ADDITIONAL_VALUES_TITLE_MARGIN_BOTTOM = 6;
const ADDITIONAL_VALUES_CONTENT_SIZE = 8;
const ALL_CATEGORY_NAME = 'All';

exports.getImageContentForOneLevelProperty = getImageContentForOneLevelProperty;
exports.getImageContentForTwoLevelProperty = getImageContentForTwoLevelProperty;
exports.getTextContent = getTextContent;
exports.getTextContentInSameLine = getTextContentInSameLine;
exports.addTextContentForSingleValue = addTextContentForSingleValue;
exports.getGoToDestinationContentForEachPage = getGoToDestinationContentForEachPage;
exports.getContentToBeAddedInEveryPage = getContentToBeAddedInEveryPage;
exports.helperMethods = {
    concatenateXAndY,
    getPropertyValue,
    getPropertyName,
    createImagesForContent,
    createBackgroundImageForContent    
}

function getImageContentForTwoLevelProperty(leafContentCallback, options) {
    return function (outputSpecProperty) {

        if (!outputSpecProperty) return getContentForEmptySection(this);

        // Filter outputSpecProperty to remove elements with no values
        outputSpecProperty = outputSpecProperty.filter(el => (el && ((el.values && el.values.length) || (el.additionalValues && Object.keys(el.additionalValues).length))))

        let contentDetails = { content: [], contentType: 'content' };
        
        //adding conditional page break
        let heightToBeWrapped = TITLE_SIZE + TITLE_MARGIN_BOTTOM + 
            SUB_TITLE_SIZE + SUB_TITLE_MARGIN_BOTTOM + 
            getHeightForFirstRow(outputSpecProperty[0], options);
        contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));

        addContentForTitle(contentDetails, this);

        // adding the rest of the content
        for (let prop of outputSpecProperty) {
            //adding conditional page break
            let heightToBeWrapped = SUB_TITLE_SIZE + SUB_TITLE_MARGIN_BOTTOM + getHeightForFirstRow(prop, options);
            contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));

            // adding title
            contentDetails.content.push(createTextContent(prop.category, SUB_TITLE_SIZE, SUB_HEADING_FONT));
            contentDetails.content.push(addContentForVerticalSpace(SUB_TITLE_MARGIN_BOTTOM));
            

            if (prop.values) {
                // adding the rest of the content
                let imagesContent = prop.values.map(convertPropertySchemaToPdfContent);
                contentDetails.content.push(leafContentCallback(imagesContent, options));
            }
            if (prop.additionalValues) pushAdditionalValuesInContent(prop.additionalValues, contentDetails);

            contentDetails.content.push(addContentForVerticalSpace(2));
        }

        return contentDetails;
    }
}


function getImageContentForOneLevelProperty(leafContentCallback, options) {
    return function (outputSpecProperty) {

        if (!outputSpecProperty) return getContentForEmptySection(this);

        let contentDetails = { content: [], contentType: 'content' };
        
        //adding conditional page break
        let heightToBeWrapped = TITLE_SIZE + TITLE_MARGIN_BOTTOM + 
            getHeightForFirstRow({values: outputSpecProperty}, options);
        contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));

        addContentForTitle(contentDetails, this);

        // adding the rest of the content
        let imagesContent = outputSpecProperty.map(convertPropertySchemaToPdfContent);
        contentDetails.content.push(leafContentCallback(imagesContent, options));
        contentDetails.content.push(addContentForVerticalSpace(2));

        return contentDetails;
    }
}

function getContentForEmptySection(_this) {
    let contentDetails = { content: [], contentType: 'content' };
    //adding conditional page break
    let heightToBeWrapped = TITLE_SIZE + TITLE_MARGIN_BOTTOM + getHeightForText('N/A', { fontSize: 10});
    contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));

    addContentForTitle(contentDetails, _this);
    
    // adding N/A
    contentDetails.content.push(addContentForNotAvailableResources());
    contentDetails.content.push(addContentForVerticalSpace(16));
    return contentDetails;
}

function addContentForTitle(contentDetails, _this) {
    //adding destination
    contentDetails.content.push(addDestination(_this.displayTitle));
    // adding title
    contentDetails.content.push(createTextContent(_this.displayTitle, TITLE_SIZE, HEADING_FONT));
    // adding margin
    contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));
}

function getTextContent(getText) {
    return function (outputSpecProperty) {
        let contentDetails = { content: [], contentType: 'content' };
        // Wrapping content for page break
        contentDetails.content.push(addContentForConditionalPageBreak(TITLE_SIZE + TITLE_MARGIN_BOTTOM + CONTENT_SIZE));
        
        //adding destination
        contentDetails.content.push(addDestination(this.displayTitle));
        //adding title
        contentDetails.content.push(createTextContent(this.displayTitle, TITLE_SIZE, HEADING_FONT));
        // adding space after title
        contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));

        if (!(outputSpecProperty && outputSpecProperty.length)) {
            // adding N/A
            contentDetails.content.push(addContentForNotAvailableResources());
            contentDetails.content.push(addContentForVerticalSpace(16));
            return contentDetails;
        } 
       
        for (let prop of outputSpecProperty) {
            let text = getText(prop);
            contentDetails.content.push(createTextContent(text, CONTENT_SIZE, TEXT_FONT));
        }

        contentDetails.content.push(addContentForVerticalSpace(16));
        return contentDetails;
    }
}

function getTextContentInSameLine(getText, separator) {
    return function (outputSpecProperty) {
        let contentDetails = { content: [], contentType: 'content' };
        // Wrapping content for page break
        contentDetails.content.push(addContentForConditionalPageBreak(TITLE_SIZE + TITLE_MARGIN_BOTTOM + CONTENT_SIZE));

        
        //adding destination
        contentDetails.content.push(addDestination(this.displayTitle));
        //adding title
        contentDetails.content.push(createTextContent(this.displayTitle, TITLE_SIZE, HEADING_FONT));
        // adding space after title
        contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));

        if (!(outputSpecProperty && outputSpecProperty.length)) {
            // adding N/A
            contentDetails.content.push(addContentForNotAvailableResources());
            contentDetails.content.push(addContentForVerticalSpace(16));
            return contentDetails;
        } 
        let text = '';
        for (let i=0; i<Object.keys(outputSpecProperty).length; i++) {
            let prop = outputSpecProperty[i];
            text += `${getText(prop)}`;
            if (i < Object.keys(outputSpecProperty).length-1) text += ` ${separator} `;
        }
        contentDetails.content.push(createTextContent(text, CONTENT_SIZE, TEXT_FONT));

        contentDetails.content.push(addContentForVerticalSpace(16));
        return contentDetails;
    }
} 

function addTextContentForSingleValue(outputSpecName) {
    let contentDetails = { content: [], contentType: 'content' };

    // Wrapping content for page break
    contentDetails.content.push(addContentForConditionalPageBreak(TITLE_SIZE + TITLE_MARGIN_BOTTOM + CONTENT_SIZE));

    //adding destination
    contentDetails.content.push(addDestination('Name'));
    //adding title
    contentDetails.content.push(createTextContent('Name', TITLE_SIZE, HEADING_FONT));
    // adding space after title
    contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));
    // adding actual name
    contentDetails.content.push(createTextContent(outputSpecName, CONTENT_SIZE, TEXT_FONT));

    contentDetails.content.push(addContentForVerticalSpace(16));
    return contentDetails;
}

function getGoToDestinationContentForEachPage(destinations) {
    return addContentsInEachPage([addGoToDestination(destinations)]);
}

function getContentToBeAddedInEveryPage(heading, destinations) {
    return addContentsInEachPage([
        addGoToDestination(destinations),
        addHeadingTitleContent(heading)
    ]);
}

function getHeightForFirstRow(prop, options) {
    let { imageHeight, imageWidth, imageGapX, imageTitleMarginTop, imageTitleMarginBottom, xMarginImages } = options;
    let cols = Math.floor((PAGE_WIDTH - (X_MARGIN_LEFT + xMarginImages + X_MARGIN_RIGHT))/(imageWidth + imageGapX));
    let heightForContent
    if (prop.values) {
        let longestImageTitleInFirstRow = '';
        for (let i=0; i<cols; i++) {
            if (i >= prop.values.length) break;
            if (prop.values[i].name.length > longestImageTitleInFirstRow) {
                longestImageTitleInFirstRow = prop.values[i].name;
            }
        }    
        heightForContent = getHeightForText(longestImageTitleInFirstRow, {width: imageWidth, fontSize: IMAGE_TITLE_FONT_SIZE});
    } else if (prop.additionalValues) {
        heightForContent = SUB_TITLE_SIZE + SUB_TITLE_MARGIN_BOTTOM + ADDITIONAL_VALUES_CONTENT_SIZE;
    }
    return imageHeight + imageTitleMarginTop + imageTitleMarginBottom + heightForContent;
}

function concatenateXAndY(prop) {
    return `${prop.x} x ${prop.y}`;
}

function getPropertyValue(prop) {
    return `${prop.value}`;
}

function getPropertyName(prop) {
    return prop.name;
}


function createImagesForContent(options) {
    return function (content) {
        return createImagesContent(content, options)
    }
}

function createBackgroundImageForContent(options) {
    return function (content) {
        return createBackgroundImagesContent(content, extractColorForBackgroundImages, options);
    }
}

function extractColorForBackgroundImages(contentInfo) {
    return extractRGBFromColorInfo(contentInfo && contentInfo.imageTitle)
}

function extractRGBFromColorInfo(rgbString) {
    try {
        return rgbString.split('(')[1].split(')')[0].split(',').map(el => +el);
    } catch(err) {
        return [0,0,0];
    }
}

function pushAdditionalValuesInContent(additionalValues, contentDetails) {
    for (let addKeys of Object.keys(additionalValues)) {
        let text = ``;
        for (let property of additionalValues[addKeys]) {
            if (text !== '') text += ', ';
            text += `${property.name}`;
            if (property.value) text += `: ${property.value}`;
        }

        //adding conditional page break
        let heightToBeWrapped = ADDITIONAL_VALUES_TITLE_SIZE + ADDITIONAL_VALUES_TITLE_MARGIN_BOTTOM + 
            getHeightForText(text, {fontSize: ADDITIONAL_VALUES_CONTENT_SIZE});
        contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));

        contentDetails.content.push(createTextContent(capitalizeString(addKeys), ADDITIONAL_VALUES_TITLE_SIZE, SUB_HEADING_FONT));
        contentDetails.content.push(addContentForVerticalSpace(ADDITIONAL_VALUES_TITLE_MARGIN_BOTTOM));
       
        contentDetails.content.push(createTextContent(text, ADDITIONAL_VALUES_CONTENT_SIZE, TEXT_FONT));
        contentDetails.content.push(addContentForVerticalSpace(16));
    }
}

function convertPropertySchemaToPdfContent(property) {
    if (!property) return;
    return {
        'path': getFilePath(property),
        'imageTitle': property && property.name,
        'fontSize': IMAGE_TITLE_FONT_SIZE,
        'skip': true,
        'font': TEXT_FONT
    };
}

function capitalizeString(str) {
    if (!str) return;
    return str.charAt(0).toUpperCase() + str.slice(1);
}