const path = require('path');
const FILE_PATH = '/tmp';
const IMAGE_FILE_PATH = path.join(FILE_PATH);

exports.getFilePath = getFilePath;

function getFilePath(property) {
    return path.join(IMAGE_FILE_PATH, extractFileName(property));
}

function extractFileName(property) {
    let fileId = (property && property.image) ? property && property.image.fileId : (property && property.fileId); 
    let fileUrl = (property && property.image) ? property && property.image.fileUrl : (property && property.fileUrl); 
    if (!(fileId && fileUrl)) return '';
    return fileId + '.' + extractImageFormatFromUrl(fileUrl);
}

function extractImageFormatFromUrl(fileUrl) {
    if (!fileUrl) return;
    let dotSplitImage = fileUrl.split('.');
    return dotSplitImage[dotSplitImage.length - 1];
}