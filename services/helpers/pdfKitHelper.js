const PAGE_WIDTH = 594;
const PAGE_HEIGHT = 842;
const X_MARGIN_LEFT = 118;
const X_MARGIN_RIGHT = 20;
const Y_MARGIN_TOP = 72;
const Y_MARGIN_BOTTOM = 72;
const TEXT_FONT = 'Roboto-Light';
const HEADING_FONT = 'Roboto-Bold';

exports.addText = addText;
exports.addImages = addImages;
exports.addTextOnLeftAndImageOnRight = addTextOnLeftAndImageOnRight;
exports.createImage = createImage;
exports.createImageBasedOnWidth = createImageBasedOnWidth;
exports.createColoredRectangle = createColoredRectangle;
exports.createRectangleWithHyperLink = createRectangleWithHyperLink;
exports.pageBreak = pageBreak;
exports.createDestination = createDestination;
exports.createGoToDestinations = createGoToDestinations;
exports.createContentsInEachPage = createContentsInEachPage;
exports.addVerticalSpace = addVerticalSpace;
exports.addHeadingTitle = addHeadingTitle;
exports.addConditionalPageBreak = addConditionalPageBreak;
exports.addContentNA = addContentNA;
exports.addTextInEachLine = addTextInEachLine;

function addText() {
    return function (doc) {
        doc.x = X_MARGIN_LEFT;
        if (this.font) doc.font(this.font);
        doc.fontSize(this.fontSize ? this.fontSize : 11)
            .text(this.text);
    }
}

function addImages(createImageCallback, { imageWidth, imageHeight, imageGapX, imageTitleMarginTop, imageTitleMarginBottom, xMarginImages }) {
    return function (doc) {
        let contentList = this.content;
        // let imageGapX = 16;
        // let imageTitleMarginTop = 8;
        // let imageTitleMarginBottom = 16;
        // let xMarginImages = 10;
        doc.x = X_MARGIN_LEFT + xMarginImages;
        let imageTitleSize = 8;

        let cols = Math.floor((PAGE_WIDTH - (X_MARGIN_LEFT + xMarginImages + 10))/(imageWidth + imageGapX));
        let rows = Math.ceil(contentList.length / cols);
        
        let contentPtr = 0;
        for (let row=0; row<rows; row++) {
            let maxImageTitleHeightForRow = -Infinity;
            for (let i=contentPtr; i<contentPtr+cols, i<contentList.length; i++) {
                let contentInfo = contentList[i];
                doc.fontSize(contentInfo.fontSize ? contentInfo.fontSize : imageTitleSize);
                let currImageTitleHeight = doc.heightOfString(contentInfo.imageTitle, { width: imageWidth });
                maxImageTitleHeightForRow = (maxImageTitleHeightForRow > currImageTitleHeight) ? maxImageTitleHeightForRow : currImageTitleHeight;
            }
            
            let contentHeightToBeAdded = imageHeight + imageTitleMarginTop + maxImageTitleHeightForRow + imageTitleMarginBottom;
            reAdjustY(doc, contentHeightToBeAdded);

            doc.x = X_MARGIN_LEFT + xMarginImages;
            let imageStartPointY = doc.y;
            for (let col=0; col<cols; col++) {
                if (contentPtr>=contentList.length) break;
                let contentInfo = contentList[contentPtr++];
                try {
                    createImageCallback(doc, imageWidth, imageHeight, contentInfo);
                } catch (er) {
                    createBlackRectangle(doc, imageWidth, imageHeight, contentInfo);
                    doc.y += imageHeight;
                }
                doc.y += imageTitleMarginTop;

                if (contentInfo.imageTitle) {
                    if (contentInfo.font) doc.font(contentInfo.font);
                    doc.fontSize(contentInfo.fontSize ? contentInfo.fontSize : imageTitleSize)
                        .text(contentInfo.imageTitle, { align: 'center', width: imageWidth });
                }
                
                doc.y = imageStartPointY;
                doc.x += imageWidth + imageGapX;
    
            }
            doc.y += contentHeightToBeAdded;
        }
        doc.x = X_MARGIN_LEFT;
    }
}

function addTextOnLeftAndImageOnRight(createImageCallback, addTextCallback, { imageWidth, imageHeight, imageTitleMarginTop, imageTitleMarginBottom }) {
    return function(doc) {
        let contentInfo = this.content;
        let prevY = doc.y;

        let lenOfHalfSection = (PAGE_WIDTH - X_MARGIN_RIGHT - X_MARGIN_LEFT)/2;
        let positionWithinHalfSection = (lenOfHalfSection - imageWidth)/2;
        doc.x = X_MARGIN_LEFT + lenOfHalfSection + positionWithinHalfSection;
        try {
            createImageCallback(doc, imageWidth, imageHeight, this.content);
        } catch (er) {
            imageHeight = 100;
            reAdjustY(doc, imageHeight);
            prevY = doc.y;
            doc.x = X_MARGIN_LEFT + lenOfHalfSection + positionWithinHalfSection;
            createBlackRectangle(doc, imageWidth, imageHeight, contentInfo);
            doc.y += imageHeight;
        }
        
        let currY = doc.y;
        let renderedImageHeight = currY - prevY; 
        doc.y = prevY;
        doc.x = X_MARGIN_LEFT;
        addTextCallback(doc, (PAGE_WIDTH - X_MARGIN_RIGHT - X_MARGIN_LEFT)/2, contentInfo);

        if (contentInfo.heightOfAllText < renderedImageHeight) {
            doc.y = currY;
        } 

    }
}

function addTextInEachLine(doc, lineWidth, contentInfo) {
    doc.fontSize(contentInfo.linesFontSize ? contentInfo.linesFontSize : 11).font(TEXT_FONT);
    for (let line of contentInfo.lines) {
        doc.text(line, { align: 'left', width: lineWidth })
    }
}

function createImage(doc, imageWidth, imageHeight, imageInfo) {
    doc.image(imageInfo.path, doc.x, doc.y, { width: imageWidth, height: imageHeight })
}

function createImageBasedOnWidth(doc, imageWidth, imageHeight, imageInfo) {
    doc.image(imageInfo.path, doc.x, doc.y, { width: imageWidth })
}

function createColoredRectangle(extractColor) {
    return function(doc, imageWidth, imageHeight, colorInfo) {
        let color = extractColor(colorInfo);
        doc.rect(doc.x, doc.y, imageWidth, imageHeight)
            .fill(color);
        doc.fill('black');
        doc.rect(doc.x, doc.y, imageWidth, imageHeight).stroke();
        reAdjustY(doc, imageHeight)
        doc.y += imageHeight;
    }
}

function createRectangleWithHyperLink(extractColor) {
    return function(doc, imageWidth, imageHeight, contentInfo) {
        let color = extractColor(contentInfo);
        if (!color) color = 'white';
        doc.rect(doc.x, doc.y, imageWidth, imageHeight)
            .fill(color);
        let prevX = doc.x, prevY = doc.y;
        doc.x += 5;
        doc.y += 5;
        doc.fontSize(contentInfo.fontSize || 11);
        if (contentInfo.fontColor) doc.fill(contentInfo.fontColor);
        if (contentInfo.imageTitleInsideRect) doc.font(TEXT_FONT).text(contentInfo.imageTitleInsideRect);
        doc.x = prevX, doc.y = prevY;

        doc.link(doc.x, doc.y, imageWidth, imageHeight, contentInfo.hyperLink);
        doc.fill('black');
        reAdjustY(doc, imageHeight)
        doc.y += imageHeight;
    }
}

function pageBreak() {
    return function(doc) {
        doc.addPage();
    }
}

function createDestination(destName) {
    return function(doc) {
        doc.addNamedDestination(destName);
    }
}

function createGoToDestinations() {
    return function(doc) {
        let destNames = this.content;
        if (!Array.isArray(destNames)) return;
        doc.x = 0;
        doc.y = Y_MARGIN_TOP;
        let menuBreakLineList = [];
        destNames.map(drawSectionBox(doc, menuBreakLineList));
        
        menuBreakLineList.pop(); // popping out the last lineBreak as it's not needed
        
        menuBreakLineList.map(addStrokeBetweenSectionMenu(doc));
        
        // let {x,y} = doc;
        // x=0, y=0;
        // let angle = -90;
        // doc.x = -PAGE_HEIGHT+Y_MARGIN_TOP;
        // doc.y = 0;
        // doc.rotate(angle, { origin: [x,y] });
        // destNames.map(drawSectionBoxVertically(doc));
        // doc.rotate(-1*angle, { origin: [x,y] });
    }
}

function createContentsInEachPage() {
    return function(doc) {
        let contents = this.content;
        let pages = doc.bufferedPageRange();
        for (let i=0; i<pages.count; i++) {
            doc.switchToPage(i);
            for (let content of contents) {
                content.addContent(doc);
            }
        }
    }
}

function addVerticalSpace(length) {
    return function(doc) {
        let glob = { isPageBreak: false }
        reAdjustY(doc, length, glob);
        if (glob.isPageBreak) return; 
        doc.y += length;
    }
}

function addHeadingTitle(heading) {
    return function(doc) {
        doc.x = 0, doc.y = 21;
        doc.font(HEADING_FONT)
            .fill('black')
            .fontSize(18)
            .text(heading, {
                width: PAGE_WIDTH,
                align: 'center'
            });
    }
}

function addConditionalPageBreak(height) {
    return function(doc) {
        reAdjustY(doc, height);
    }
}

function addContentNA() {
    return function(doc) {
        doc
            .fontSize(10)
            .font(TEXT_FONT)
            .text('N/A');
    }
}

function createBlackRectangle(doc, imageWidth, imageHeight) {
    doc.rect(doc.x, doc.y, imageWidth, imageHeight)
        .fill('grey');
    doc.fill('black');
}

function reAdjustX(doc, contentWidth, contentHeight, glob) {
    if (doc.x + contentWidth > (PAGE_WIDTH - X_MARGIN_RIGHT)) {
        resetX(doc);
        doc.y += contentHeight;
        reAdjustY(doc, contentHeight);
        glob.isLineBreak = true;
    }
}

function reAdjustY(doc, contentHeight, glob) {
    if (doc.y + contentHeight > (PAGE_HEIGHT - Y_MARGIN_BOTTOM - 10)) {
        if (glob) glob.isPageBreak = true;
        doc.addPage();
        doc.x = X_MARGIN_LEFT;
        doc.y = Y_MARGIN_TOP;
    }
}

function resetX(doc) {
    doc.x = X_MARGIN_LEFT;
}

function drawSectionBox(doc, menuBreakLineList) {
    return function(destName) {
        let padding = 8;
        let width = 94;
        let oldY = doc.y;
        doc.fontSize(8);
        let textHeight = doc.heightOfString(destName, { width }); // height of the text getting added
        doc.y += padding;
        let yForText = doc.y;
        
        doc.goTo(0, oldY, width, textHeight + 2*padding, destName);
        doc.rect(0, oldY, width, textHeight + 2*padding).fill("#f9f9f9");
        

        doc.fillColor('#808080')
            // .fontSize(8)
            .font(TEXT_FONT)
            .text(destName, padding, yForText, {
                width: width-padding,
                align: 'left'
            }
        );
        
        doc.y += padding;
        
        if (menuBreakLineList) menuBreakLineList.push({moveTo: [doc.x, doc.y], lineTo: [width - padding, doc.y]});

    }
}

function addStrokeBetweenSectionMenu(doc) { 
    return function(lineStrokeDetails) {
        doc.strokeColor('#DADADA');
        doc.moveTo(...lineStrokeDetails.moveTo);
        doc.lineTo(...lineStrokeDetails.lineTo).stroke();   
    }
}

// function drawSectionBoxVertically(doc) {
//     return function(destName) {
//         let width = 60;
//         doc.fontSize(10)
//             .text(destName, doc.x, 0, {
//                 width,
//                 align: 'center'
//             }
//         );
//         doc.goTo(doc.x, 0, width, doc.y, destName);
//         doc.rect(doc.x, 0, width, doc.y).stroke();
//         doc.x += width
//     }
// }