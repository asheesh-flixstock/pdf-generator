const { addText, addImages, addTextOnLeftAndImageOnRight, createImage, createImageBasedOnWidth, createColoredRectangle, createRectangleWithHyperLink, pageBreak, createDestination, createGoToDestinations, createContentsInEachPage, addVerticalSpace, addHeadingTitle, addConditionalPageBreak, addContentNA, addTextInEachLine } = require('./pdfKitHelper.js');


exports.createTextContent = createTextContent;
exports.createImagesContent = createImagesContent;
exports.createImageOnRightContent = createImageOnRightContent;
exports.createBackgroundImagesContent = createBackgroundImagesContent;
exports.createRectangularBoxesWithHyperLinks = createRectangularBoxesWithHyperLinks;
exports.addPageBreak = addPageBreak;
exports.addDestination = addDestination;
exports.addGoToDestination = addGoToDestination;
exports.addContentsInEachPage = addContentsInEachPage;
exports.addContentForVerticalSpace = addContentForVerticalSpace;
exports.addHeadingTitleContent = addHeadingTitleContent;
exports.addContentForConditionalPageBreak = addContentForConditionalPageBreak;
exports.addContentForNotAvailableResources = addContentForNotAvailableResources; 

/**
 * 
 * @param {String} text 
 * @param {Number} fontSize 
 */
function createTextContent(text, fontSize, font) {
    return { text, fontSize, font, contentType: 'text', addContent: addText() };
}

function createImageOnRightContent(imageInfo, options) {
    return { content: imageInfo, contentType: 'singleImage', addContent: addTextOnLeftAndImageOnRight(createImageBasedOnWidth, addTextInEachLine, options) }
}

/**
 * 
 * @param {Array} content 
 * @param {Number} width 
 * @param {Number} height 
 */
function createImagesContent(content, options) {
    return { content, contentType: 'image', addContent: addImages(createImage, options) };
}

/**
 * 
 * @param {Array} content 
 * @param {Number} width 
 * @param {Number} height 
 */
function createBackgroundImagesContent(content, extractColorCallback, options) {
    return { content, contentType: 'image', addContent: addImages(createColoredRectangle(extractColorCallback), options) };
}

function createRectangularBoxesWithHyperLinks(content, extractColorCallback, options) {
    return { content, contentType: 'image', addContent: addImages(createRectangleWithHyperLink(extractColorCallback), options) };
}

function addPageBreak() {
    return { content: null, contentType: 'pageBreak', addContent: pageBreak() }
}

function addDestination(destinationName) {
    return { content: null, contentType: 'addDestination', addContent: createDestination(destinationName) }
}

function addGoToDestination(destinationNames) {
    return { content: destinationNames, contentType: 'goToDestination', addContent: createGoToDestinations() }
}

function addContentsInEachPage(content) {
    return { content, contentType: 'addInEachPage', addContent: createContentsInEachPage() }
}

function addContentForVerticalSpace(length) {
    return { content: null, contentType: 'verticalSpace', addContent: addVerticalSpace(length) };
}

function addHeadingTitleContent(heading) {
    return { content: null, contentType: 'heading', addContent: addHeadingTitle(heading) };
}

function addContentForConditionalPageBreak(height) {
    return { content: null, contentType: 'conditionalPageBreak', addContent: addConditionalPageBreak(height) };
}

function addContentForNotAvailableResources() {
    return { content: null, contentType: 'na', addContent: addContentNA() };
}