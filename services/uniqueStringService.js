const uniqid = require('/opt/uniqid'); 

exports.generateTenDigitUniqueString = generateTenDigitUniqueString;

function generateTenDigitUniqueString() {
    let randomString = Math.random().toString(36);
    let randomTwoChar = randomString.slice(randomString.length-2);
    return uniqid.time() + randomTwoChar;
}