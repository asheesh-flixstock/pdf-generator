const PDFDocument = require('/opt/pdfkit');
const fs = require('fs');

exports.pdfkit = pdfkit;
exports.getHeightForText = getHeightForText; 

function pdfkit(pdfFileName, content) {
    return new Promise((resolve, reject) => {
        const doc = new PDFDocument({ size: 'A4', bufferPages: true });
        let writeStream = fs.createWriteStream(pdfFileName);
        doc.pipe(writeStream);
        
        // Registering Roboto fonts
        doc.registerFont('Roboto-Bold', '/opt/Roboto/Roboto-Bold.ttf');
        doc.registerFont('Roboto-Light', '/opt/Roboto/Roboto-Light.ttf');

        writeStream.on('finish', () => {
            resolve()
        });    
        addContent(doc, content);
        doc.end();   
    })
} 

function getHeightForText(text, { width, fontSize }) {
    const doc = new PDFDocument({});
    if (fontSize) doc.fontSize(fontSize);
    let height = doc.heightOfString(text, { width });
    doc.end();   
    return height;
}

function addContent(doc, content) {
    let stack = [];
    stack.push(content);
    while (stack.length) {
        let poppedContent = stack.pop();
        if (!poppedContent) continue;
        if ((poppedContent.skip)) continue;
        if (poppedContent.contentType !== 'content' && poppedContent.addContent) {
            poppedContent.addContent(doc);
        }
        if (!poppedContent.content) continue;
        for (let i=poppedContent.content.length-1; i>-1; i--) stack.push(poppedContent.content[i]) 
    }
}
