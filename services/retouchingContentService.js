const { createTextContent, createImagesContent, createImageOnRightContent, createBackgroundImagesContent, addPageBreak, addDestination, addGoToDestination, addContentsInEachPage, addContentForVerticalSpace, addHeadingTitleContent, addContentForConditionalPageBreak, addContentForNotAvailableResources } = require('../services/helpers/contentHelper.js');
const { getHeightForText } = require('./pdfKitService');
const { getFilePath } = require('./filePathService');
const { getImageHeightBasedOnWidth } = require('./imageSizeService');
const PAGE_WIDTH = 594;
const X_MARGIN_LEFT = 118;
const X_MARGIN_RIGHT = 10;
const TITLE_SIZE = 14;
const TITLE_MARGIN_BOTTOM = 10;
const MODEL_TYPE_SIZE = 12;
const MODEL_TYPE_MARGIN_BOTTOM = 8;
const MODEL_TEXT_SIZE = 10;
const MODEL_TEXT_MARGIN_BOTTOM = 5;
const IMAGE_TITLE_FONT_SIZE = 8;
const HEADING_FONT = 'Roboto-Bold';
const TEXT_FONT = 'Roboto-Light';

exports.getContentForRetouching = getContentForRetouching;

function getContentForRetouching(options) {
    return async function(modelListByType) {
        let contentDetails = { contentType: 'content', content: [] };
        if (!modelListByType) return contentDetails;
        let keys = Object.keys(modelListByType).sort();

        let retouchingExists = false;
        for (let key of keys) {
            let modelsByType = modelListByType[key];
            for (let model of modelsByType) {
                if (model.retouching) {
                    retouchingExists = true;
                    break;
                }
            }
        }

        if (!retouchingExists) {
            let heightToBeWrapped = 
                TITLE_SIZE + TITLE_MARGIN_BOTTOM +
                getHeightForText('N/A', { fontSize: 10});
            contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));
        
            contentDetails.content.push(addDestination(this.displayTitle));
            
            contentDetails.content.push(createTextContent(this.displayTitle, TITLE_SIZE, HEADING_FONT));
            contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));
            contentDetails.content.push(addContentForNotAvailableResources());
            contentDetails.content.push(addContentForVerticalSpace(16));
            return contentDetails;
        }


        
        let firstModelList = modelListByType && modelListByType[keys[0]] ? modelListByType[keys[0]] : [];
        let heightToBeWrapped = 
            TITLE_SIZE + TITLE_MARGIN_BOTTOM +
            MODEL_TYPE_SIZE + MODEL_TYPE_MARGIN_BOTTOM + 
            MODEL_TEXT_SIZE + MODEL_TEXT_MARGIN_BOTTOM +
            getHeightForFirstRowOfModels(firstModelList, options);
        contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));
    
        contentDetails.content.push(addDestination(this.displayTitle));
        
        contentDetails.content.push(createTextContent(this.displayTitle, TITLE_SIZE, HEADING_FONT));
        contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));
    
        
        await addContentForModelsOfAllTypes(modelListByType, options, contentDetails);
        
        console.log("Retouching content --> ", JSON.stringify(contentDetails));
        
        return contentDetails;
    }
}

function getHeightForFirstRowOfModels(modelList, options) {
    // let modelList = modelListByType[Object.keys(modelListByType)[0]];
    let { imageHeight, imageWidth, imageGapX, imageTitleMarginTop, imageTitleMarginBottom, xMarginImages } = options;
    let cols = Math.floor((PAGE_WIDTH - (X_MARGIN_LEFT + xMarginImages + X_MARGIN_RIGHT))/(imageWidth + imageGapX));
    let heightForContent;

    // Finding out the longest name
    let longestImageTitleInFirstRow = '';
    for (let i=0; i<cols; i++) {
        if (i >= modelList.length) break;
        if (!(modelList[i] && modelList[i].modelName)) continue;
        if (modelList[i].modelName.length > longestImageTitleInFirstRow) {
            longestImageTitleInFirstRow = modelList[i].modelName;
        }
    }    

    heightForContent = getHeightForText(longestImageTitleInFirstRow, {width: imageWidth, fontSize: IMAGE_TITLE_FONT_SIZE});
    return imageHeight + imageTitleMarginTop + imageTitleMarginBottom + heightForContent;
}


async function addContentForModelsOfAllTypes(modelListByType, options, contentDetails) {
    let keys = Object.keys(modelListByType).sort();
    for (let modelByTypeKey of keys) {
        let modelByType = modelListByType[modelByTypeKey];

        let firstModelList = modelByType && modelByType[0] && modelByType[0].models ? modelByType[0].models : [];
        let heightToBeWrappedForType = 
            MODEL_TYPE_SIZE + MODEL_TYPE_MARGIN_BOTTOM + 
            MODEL_TEXT_SIZE + MODEL_TEXT_MARGIN_BOTTOM +
            getHeightForFirstRowOfModels(firstModelList, options);
        contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrappedForType));

        let modelTypeWithCapitalCase = modelByTypeKey.charAt(0).toUpperCase() + modelByTypeKey.slice(1);
        contentDetails.content.push(createTextContent(modelTypeWithCapitalCase, MODEL_TYPE_SIZE, HEADING_FONT));
        contentDetails.content.push(addContentForVerticalSpace(MODEL_TYPE_MARGIN_BOTTOM));
        
        
        for (let modelByRetouching of modelByType) {
            let modelList = modelByRetouching.models;
            let retouchingDetails = modelByRetouching.retouching;
            
            if (!(retouchingDetails && modelList)) continue;
            
            modelList.sort(compareModelsByRetouching);
            let heightToBeWrappedForText = 
                MODEL_TEXT_SIZE + MODEL_TEXT_MARGIN_BOTTOM +
                getHeightForFirstRowOfModels(modelList, options);
            contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrappedForText));
            
            contentDetails.content.push(createTextContent('Apply same retouching for all the models.', MODEL_TEXT_SIZE, TEXT_FONT));
            contentDetails.content.push(addContentForVerticalSpace(MODEL_TEXT_MARGIN_BOTTOM));
            
            contentDetails.content.push(createImagesContent(modelList.map(convertModelIntoPdfContent), options));

            options.retouchingWidth = 150; // Width of the retouching image
            
            let heightToBeWrappedForRetouching = retouchingDetails ? await getImageHeightBasedOnWidth(getFilePath(retouchingDetails.image), options.retouchingWidth) : 0;
            contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrappedForRetouching));
            
            contentDetails.content.push(createTextContent('Retouching', 10, HEADING_FONT));
            contentDetails.content.push(addContentForVerticalSpace(MODEL_TYPE_MARGIN_BOTTOM));
            
            contentDetails.content.push(createImageOnRightContent(convertRetouchingIntoPdfContent(retouchingDetails), { imageWidth: options.retouchingWidth }));
            
            contentDetails.content.push(addContentForVerticalSpace(16));
        }
    }
}

function convertModelIntoPdfContent(modelDetails) {
    if (!modelDetails) return;
    return {
        'path': getFilePath(modelDetails.image),
        'imageTitle': modelDetails.modelName,
        'fontSize': IMAGE_TITLE_FONT_SIZE,
        'skip': true,
        'font': TEXT_FONT
    };
}

function convertRetouchingIntoPdfContent(retouchingDetails) {
    if (!retouchingDetails) return;
    return {
        'path': getFilePath(retouchingDetails.image),
        'imageTitleFontSize': 8,
        'imageTitle': retouchingDetails.name,
        'fontSize': 11,
        'skip': true,
        'font': TEXT_FONT,
        'lines': extractCommentsFromRetouchingDetails(retouchingDetails),
        'heightOfAllText': getHeightOfAllComments(retouchingDetails),
        'linesFontSize': 11 
    };
}

function extractCommentsFromRetouchingDetails(retouchingDetails) {
    let comments = retouchingDetails && retouchingDetails.comments;
    if (!comments) return [];
    comments.sort(compareComments);
    return comments.map(el => `${el.index}. ${el.comment}`);
}

function getHeightOfAllComments(retouchingDetails) {
    let comments = extractCommentsFromRetouchingDetails(retouchingDetails);
    let totalHeight = 0;
    for (let comment of comments) {
        totalHeight += getHeightForText(comment, { fontSize: 11, width: 400 });
    }
    return totalHeight;
}

function compareComments(com1, com2) {
    let comIndex1 = +com1.index;
    let comIndex2 = +com2.index;
    return comIndex1 - comIndex2;
}

function compareModelsByRetouching(mod1, mod2) {
    if (!mod1.retouching) return 0;
    if (!mod2.retouching) return 1;
    return mod1.retouching._id < mod2.retouching._id ? -1 : 1 ;
}