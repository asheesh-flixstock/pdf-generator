const { createTextContent, createRectangularBoxesWithHyperLinks, addContentForVerticalSpace, addDestination, addContentForConditionalPageBreak, addContentForNotAvailableResources } = require('../services/helpers/contentHelper.js');
const { getHeightForText } = require('./pdfKitService');
const { getFilePath } = require('./filePathService');
const PAGE_WIDTH = 594;
const X_MARGIN_LEFT = 118;
const X_MARGIN_RIGHT = 10;
const TITLE_SIZE = 14;
const TITLE_MARGIN_BOTTOM = 10;
const IMAGE_TITLE_FONT_SIZE = 8;
const HEADING_FONT = 'Roboto-Bold';
const TEXT_FONT = 'Roboto-Light';

exports.getContentForBrandDocument = getContentForBrandDocument;

function getContentForBrandDocument(options) {
    return function(brandDocuments) {
        let contentDetails = { contentType: 'content', content: [] };

        if (!(brandDocuments && brandDocuments.length)) {
            let heightToBeWrapped = TITLE_SIZE + TITLE_MARGIN_BOTTOM + getHeightForText('N/A', { fontSize: 10});
            contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));
            contentDetails.content.push(addDestination(this.displayTitle));
            contentDetails.content.push(createTextContent('About the Brand', TITLE_SIZE, HEADING_FONT));
            contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));
            contentDetails.content.push(addContentForNotAvailableResources());
            contentDetails.content.push(addContentForVerticalSpace(16));
            return contentDetails;
        }

        let heightToBeWrapped = TITLE_SIZE + TITLE_MARGIN_BOTTOM + getHeightForFirstRow(brandDocuments, options);
        contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));
    
        contentDetails.content.push(addDestination(this.displayTitle));
        
        contentDetails.content.push(createTextContent('About the Brand', TITLE_SIZE, HEADING_FONT));
        contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));
    
        contentDetails.content.push(getContentForBrandDocFiles(brandDocuments, options));
        return contentDetails;    
    }
}

function getHeightForFirstRow(brandDocuments, options) {
    let { imageHeight, imageWidth, imageGapX, imageTitleMarginTop, imageTitleMarginBottom, xMarginImages } = options;
    let cols = Math.floor((PAGE_WIDTH - (X_MARGIN_LEFT + xMarginImages + X_MARGIN_RIGHT))/(imageWidth + imageGapX));
    let heightForContent;
    let longestImageTitleInFirstRow = '';
    for (let i=0; i<cols; i++) {
        if (i >= brandDocuments.length) break;
        if (!(brandDocuments[i] && brandDocuments[i].fileName)) continue;
        if (brandDocuments[i].fileName.length > longestImageTitleInFirstRow) {
            longestImageTitleInFirstRow = brandDocuments[i].fileName;
        }
    }    
    heightForContent = getHeightForText(longestImageTitleInFirstRow, {width: imageWidth, fontSize: IMAGE_TITLE_FONT_SIZE});
    return imageHeight + imageTitleMarginTop + imageTitleMarginBottom + heightForContent;
}

function getContentForBrandDocFiles(brandDocuments, options) {
    let content = [];
    content.push(...brandDocuments.map(convertBrandDocumentToPdfContent));
    return createRectangularBoxesWithHyperLinks(content, getRectangleBackground, options)
}

function getRectangleBackground() {
    return `#E2E5E7`;
}

function convertBrandDocumentToPdfContent(property) {
    return {
        'path': getFilePath(property),
        'imageTitleInsideRect': extractFileType(property),
        'imageTitle': property && property.fileName,
        'fontSize': IMAGE_TITLE_FONT_SIZE,
        'fontColor': 'black',
        'skip': true,
        'font': TEXT_FONT,
        'hyperLink': property && property.fileUrl
    };
}

function extractFileType(property) {
    try {
        return property.fileName.split('.').pop().toUpperCase();
    } catch(er) {
        return '';
    }
}