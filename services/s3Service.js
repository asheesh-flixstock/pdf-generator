const { S3 }= require('../config/amazonWebServices');
const fs = require('fs');
const s3 = new S3();

exports.uploadFileToS3 = uploadFileToS3;
exports.removeFileFromS3UsingUrl = removeFileFromS3UsingUrl;

function removeFileFromS3UsingUrl(fileUrl, bucket) {
    if (!fileUrl) return;
    let key = extractKeyFromUrl(fileUrl);
    if (!key) return;
    let params = {
        Bucket: bucket,
        Key: key
    };
    return new Promise((resolve, reject) => {
        s3.deleteObject(params, (err, data) => {
            if (err) console.log("Error - unable to delete previous pdf url from S3");
            else console.log("Previous pdf File deleted from S3");
            resolve();
        })
    })
} 

async function uploadFileToS3(filePath, key, bucket) {
    let file = fs.readFileSync(filePath);
    let params = {
        Bucket: bucket,
        Key: key,
        Body: file
    };
    
    return new Promise((resolve, reject) => {
        return s3.upload(params, (err, data) => {
            if (err) return reject(err);
            resolve(data.Location);
        });
    })
}

function extractKeyFromUrl(fileUrl) {
    if (!fileUrl) return;
    try {
        let splitFileUrl = fileUrl.split('/');
        for (let i=0; i<3; i++) splitFileUrl.shift();
        return splitFileUrl.join('/');
    } catch(er) {
        return;
    }
}