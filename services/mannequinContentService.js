const { createTextContent, createImagesContent, addContentForVerticalSpace, addContentForNotAvailableResources, addDestination, addContentForConditionalPageBreak } = require('../services/helpers/contentHelper.js');
const { getFilePath } = require('./filePathService');
const { getHeightForText } = require('./pdfKitService');
const imageKeys = [
    { "keyName": 'mannequinFrontInfo', 'imageTitle': 'Front' },
    { "keyName": 'mannequinBackInfo', 'imageTitle': 'Back' },
    { "keyName": 'mannequinSideInfo', 'imageTitle': 'Side' }
];
const HEADING_FONT = 'Roboto-Bold';
const SUB_HEADING_FONT = 'Roboto-Bold';
const TEXT_FONT = 'Roboto-Light';
const TITLE_SIZE = 14;
const TITLE_MARGIN_BOTTOM = 10;
const TARGET_GROUP_NAME_SIZE = 12;
const TARGET_GROUP_NAME_MARGIN_BOTTOM = 6;
const MANNEQUIN_TYPE_TITLE_SIZE = 10;
const MANNEQUIN_TYPE_MARGIN_BOTTOM = 4;
const MANNEQUIN_NAME_SIZE = 8;
const MANNEQUIN_NAME_MARGIN_BOTTOM = 2;
const IMAGE_TITLE_FONT_SIZE = 8;

exports.getContentForMannequin = getContentForMannequin;

function getContentForMannequin(options) {
    return function(mannequinData) {
        mannequinData = mannequinData && mannequinData[0] && mannequinData[0].mannequin;
        if (!mannequinData) return Promise.resolve();
        let contentDetails = { contentType: 'content', content: [] };
        
        let heightToBeWrapped = TITLE_SIZE + TITLE_MARGIN_BOTTOM +
            TARGET_GROUP_NAME_SIZE + TARGET_GROUP_NAME_MARGIN_BOTTOM + 
            MANNEQUIN_TYPE_TITLE_SIZE + MANNEQUIN_TYPE_MARGIN_BOTTOM +
            MANNEQUIN_NAME_SIZE + MANNEQUIN_NAME_MARGIN_BOTTOM +
            getHeightForFirstRow(options);
        contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));
        
        contentDetails.content.push(addDestination(this.displayTitle));
    
        contentDetails.content.push(createTextContent('Mannequin', TITLE_SIZE, HEADING_FONT));
        contentDetails.content.push(addContentForVerticalSpace(TITLE_MARGIN_BOTTOM));
    
        for (let targetGroupName of Object.keys(mannequinData)) addMannequinDetailsIntoContent(mannequinData, contentDetails, targetGroupName, options);
        return contentDetails;    
    }
}

function addMannequinDetailsIntoContent(mannequinData, contentDetails, targetGroupName, options) {
    if (!(mannequinData && mannequinData[targetGroupName] && mannequinData[targetGroupName].length)) {
        contentDetails.content.push(addContentForNotAvailableResources());
        contentDetails.content.push(addContentForVerticalSpace(16));
        return;
    }
    let heightToBeWrapped = 
        TARGET_GROUP_NAME_SIZE + TARGET_GROUP_NAME_MARGIN_BOTTOM + 
        MANNEQUIN_TYPE_TITLE_SIZE + MANNEQUIN_TYPE_MARGIN_BOTTOM +
        MANNEQUIN_NAME_SIZE + MANNEQUIN_NAME_MARGIN_BOTTOM +
        getHeightForFirstRow(options);
    contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));
    
    contentDetails.content.push(createTextContent(capitalizeString(targetGroupName), TARGET_GROUP_NAME_SIZE, SUB_HEADING_FONT));
    contentDetails.content.push(addContentForVerticalSpace(TARGET_GROUP_NAME_MARGIN_BOTTOM));
    let types = getUniqueTypes(mannequinData[targetGroupName]);
    for (let type of types) addMannequinTypeIntoContent(mannequinData, contentDetails, targetGroupName, type, options);
}

function addMannequinTypeIntoContent(mannequinData, contentDetails, targetGroupName, type, options) {
    if (!(mannequinData && mannequinData[targetGroupName])) return;
    let mannequinByType = mannequinData[targetGroupName].filter(mann => mann.type == type);
    if (!mannequinByType.length) return;

    let heightToBeWrapped = MANNEQUIN_TYPE_TITLE_SIZE + MANNEQUIN_TYPE_MARGIN_BOTTOM +
        MANNEQUIN_NAME_SIZE + MANNEQUIN_NAME_MARGIN_BOTTOM +
        getHeightForFirstRow(options);
    contentDetails.content.push(addContentForConditionalPageBreak(heightToBeWrapped));

    
    contentDetails.content.push(createTextContent(capitalizeString(type), MANNEQUIN_TYPE_TITLE_SIZE, SUB_HEADING_FONT));
    contentDetails.content.push(addContentForVerticalSpace(MANNEQUIN_TYPE_MARGIN_BOTTOM));
    for (let mann of mannequinByType) {
        contentDetails.content.push(createTextContent(mann.mannequinName, MANNEQUIN_NAME_SIZE, SUB_HEADING_FONT));
        contentDetails.content.push(addContentForVerticalSpace(MANNEQUIN_NAME_MARGIN_BOTTOM));
        contentDetails.content.push(createImagesContent(extractMannequinImages(mann), options));
    }
    contentDetails.content.push(addContentForVerticalSpace(8));

}

function capitalizeString(str) {
    if (!str) return;
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function getUniqueTypes(mannequinTargetGroup) {
    if (!mannequinTargetGroup) return [];
    let types = {};
    for (let mann of mannequinTargetGroup) types[mann.type] = true;
    return Object.keys(types);
}

function extractMannequinImages(mannequin) {
    if (!(mannequin && mannequin.mannequinData)) return;
    let images = [];
    
    for (let key of imageKeys) {
        if (!mannequin.mannequinData[key.keyName]) continue;
        images.push(convertPropertyToPdfContent(mannequin.mannequinData[key.keyName], key.imageTitle));
    }
    return images;
}

function convertPropertyToPdfContent(property, imageTitle) {
    return {
        'path': getFilePath(property),
        imageTitle,
        'fontSize': IMAGE_TITLE_FONT_SIZE,
        'skip': true,
        'font': TEXT_FONT
    };
}

function getHeightForFirstRow(options) {
    let { imageHeight, imageWidth, imageGapX, imageTitleMarginTop, imageTitleMarginBottom, xMarginImages } = options;
    let heightForContent;
    let longestImageTitleInFirstRow = '';
    for (let key of imageKeys) {
        if (key.imageTitle.length > longestImageTitleInFirstRow) {
            longestImageTitleInFirstRow = key.imageTitle;
        }
    }    
    heightForContent = getHeightForText(longestImageTitleInFirstRow, {width: imageWidth, fontSize: IMAGE_TITLE_FONT_SIZE});
    return imageHeight + imageTitleMarginTop + imageTitleMarginBottom + heightForContent;
}