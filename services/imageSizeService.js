const { promisify } = require('util')
const sizeOf = promisify(require('/opt/image-size'));

exports.getImageHeightBasedOnWidth = getImageHeightBasedOnWidth;

async function getImageHeightBasedOnWidth(imagePath, imageWidth) {
    try {
        let {width, height} = await sizeOf(imagePath);
        return Math.ceil(height*imageWidth/width);
    } catch(er) {
        return 100;
    }   
}
