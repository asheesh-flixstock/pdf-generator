const path = require('path');
const fs = require('fs');
// require('/opt/dotenv').config({ path: `${__dirname}/process.env` })
const { generatePdfForMannequin } = require('./pdfGenerators/pdfGeneratorMannequin');
const { generatePDFForOutputSpec } = require('./pdfGenerators/pdfGeneratorOutputSpec');
const { generateGenericPdf } = require('./pdfGenerators/pdfGeneratorGeneric');

exports.handler = handler;

async function handler(event, context, callback) {
    try {
        let pdfUrl;
        pdfUrl = await generateGenericPdf(event.queryStringParameters.type, event.queryStringParameters.projectId, event.queryStringParameters.clientId, event.queryStringParameters.outputSpecId);
        // switch (event.queryStringParameters.type) {
        //     case 'outputSpecification':
        //         pdfUrl = await generatePDFForOutputSpec(event.queryStringParameters.clientId, event.queryStringParameters.outputSpecId);
        //         break;
        //     case 'mannequin':
        //         pdfUrl = await generatePdfForMannequin(event.queryStringParameters.clientId);
        //         break;
        //     case 'generic':
        //         pdfUrl = await generateGenericPdf(event.queryStringParameters.projectId, event.queryStringParameters.clientId, event.queryStringParameters.outputSpecId);
        //         break;
        // }
        clearTmpDirectory();
        // callback(pdfUrl, 200);
        return {
            statusCode: 200,
            body: pdfUrl
        };
    } catch (er) {
        console.log(er);
        return {
            statusCode: 500,
            body: JSON.stringify(er)
        };
    }
}

function clearTmpDirectory() {
    console.log('Clearing tmp directory....');
    let dir = '/tmp';
    fs.readdir(dir, (err, files) => {
        if (err) throw err;
        for (let fileName of files) {
            console.log(fileName);
            fs.unlink(path.join(dir, fileName), err => {
                if (err) throw err;
            });
        }
    });
}