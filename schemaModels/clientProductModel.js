
var { Schema, Type, addModel } = require('../config/database'); 

const ClientProductSchema = new Schema({
    name: { type: String, required: true },
    projectId: { type: Type.ObjectId, required: true },
    createdAt : { type: Date, default: Date.now, required: true },
    createdBy : { type: Type.ObjectId, required: true },
    updatedAt : { type: Date },
    updatedBy : { type: Type.ObjectId },
    productList: { type: Array },
    productHierarchy: { type: Array},
    productType: { type: String, required: true },
    customSelectedValue: { 
        type: [{
            type: { type: String },
            value: { type: String }
        }]
    },
    mannequinSelection: { type: [Object] },
    outputSpecification: { type: [Object] },
    model: { type: Object }
});


ClientProductSchema.statics.getSelectedOutputSpecification = getSelectedOutputSpecification;
ClientProductSchema.statics.getAllSelectedMannequins = getAllSelectedMannequins;


function getSelectedOutputSpecification(projectId, clientProductId, outputSpecId) {
    return ClientProductModel.findOne({projectId: Type.ObjectId(projectId), _id: Type.ObjectId(clientProductId), outputSpecification: { $elemMatch: { _id: Type.ObjectId(outputSpecId) } } }, { 'outputSpecification.$': 1 })
        .lean()
        .then((doc) => doc && doc.outputSpecification && doc.outputSpecification[0]);
}

/**
 * Gets list of selected mannequins for a given client
 * @author Hirdayam
 * @param {String} clientProductId 
 */

function getAllSelectedMannequins(clientProductId) {
    return ClientProductModel.aggregate([
        { "$match": {"_id": Type.ObjectId(clientProductId)}},
        {"$project": {"mannequin":{
            "$map": {
                "input": "$mannequinSelection",
                "in": {
                     "k":"$$this.targetGroup", "v":"$$this.mannequinList"
                 }
              }
           }
         }
       },
       {
             "$project": {
                     "mannequin": {
                             "$arrayToObject":  "$mannequin"
                         
                      }
              }
        }
    ])
}

const ClientProductModel = addModel('ClientProductMaster', ClientProductSchema);
module.exports = ClientProductModel;
