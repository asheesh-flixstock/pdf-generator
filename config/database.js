const { MongoClient, ObjectId } = require('/opt/mongodb');

const url = require('./config').DB_CONNECTION;
const client = new MongoClient(url)
const PDF_TYPES = {
    'OUTPUT_SPECIFICATION': 'outputSpecification',
    'MANNEQUIN': 'mannequin',
    'GENERIC': 'generic',
    'TEMPLATE': 'template',
    'DRAPING': 'draping',
    'GARMENT_RETOUCHING': 'garmentRetouching'
}

exports.getClientName = getClientName;
exports.getSelectedOutputSpecification = getSelectedOutputSpecification;
exports.getAllSelectedMannequins = getAllSelectedMannequins;
exports.getBrandDocument = getBrandDocument;
exports.getModelsWithRetouching  = getModelsWithRetouching;
exports.getPdfContentByType = getPdfContentByType;
exports.savePdfUrl = savePdfUrl;

const dbName = 'client-onboarding';
const clientProductCollection = 'clientproductmasters';
const clientProductMetadataCollection = 'clientproductmetadatas';
const projectCollection = 'projectmasters';
const pdfMetadataCollection = 'pdfmetadatas';
const pdfStorageCollection = 'pdfstorages'

async function connectAndGetCollection(dbName, collectionName) {
  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db(dbName);
  const collection = db.collection(collectionName);
  return collection;
}

async function getClientName(clientProductId) {
    let collectionConn = await connectAndGetCollection(dbName, clientProductCollection);
    return collectionConn.find({_id: ObjectId(clientProductId)}, { 'name': 1 })
        .toArray()
        .then((doc) => doc && doc[0] && doc[0].name);
}

async function getSelectedOutputSpecification(clientProductId, outputSpecId) {
    let collectionConn = await connectAndGetCollection(dbName, clientProductCollection);
    return collectionConn.find({_id: ObjectId(clientProductId), outputSpecification: { $elemMatch: { _id: ObjectId(outputSpecId) } } }, { 'outputSpecification': 1 })
        .toArray()
        .then((doc) => {
            let outputSpec = doc && doc[0] && doc[0].outputSpecification;
            if (!outputSpec) return;
            return outputSpec.find(el => {
                return el._id == (outputSpecId)
            });
        })
        .then(addCustomKeys)
}

/**
 * Gets list of selected mannequins for a given client
 * @author Hirdayam
 * @param {String} clientProductId 
 */

async function getAllSelectedMannequins(clientProductId) {
    let collectionConn = await connectAndGetCollection(dbName, clientProductCollection);
    return collectionConn.aggregate([
        { "$match": {"_id": ObjectId(clientProductId)}},
        {"$project": {"mannequin":{
            "$map": {
                "input": "$mannequinSelection",
                "in": {
                     "k":"$$this.targetGroup", "v":"$$this.mannequinList"
                 }
              }
           }
         }
       },
       {
             "$project": {
                     "mannequin": {
                             "$arrayToObject":  "$mannequin"
                         
                      }
              }
        }
    ]).toArray();
}

async function getBrandDocument(projectId) {
    let collectionConn = await connectAndGetCollection(dbName, projectCollection);
    return collectionConn.find({_id: ObjectId(projectId) }, { 'brandDocument': 1 })
        .toArray()
        .then((doc) => doc && doc[0] && doc[0].brandDocument);
}

async function getModelsWithRetouching(clientProductId, targetGroupId) {
    let models = await getAllModelGroupedByTypeAndRetouching(clientProductId, targetGroupId);
    let retouchingHash = await getAllRetouchingForClient(clientProductId, targetGroupId);
    for (let modelType of Object.keys(models)) {
        let modelList = models[modelType];
        for (let model of modelList) {
            if (!model.retouching) continue;
            model.retouching.image = retouchingHash[model.retouching._id] && retouchingHash[model.retouching._id].image;
            model.retouching.comments = retouchingHash[model.retouching._id] && retouchingHash[model.retouching._id].comments;
        }
    }
    return models;
}

async function getAllModelGroupedByTypeAndRetouching(clientProductId, targetGroupId) {
    let collectionConn = await connectAndGetCollection(dbName, clientProductCollection);
    return collectionConn.aggregate([
        { "$match": {_id: ObjectId(clientProductId)  } },
        { "$project": { "targetGroup":{"$arrayElemAt":[{"$filter":{"input":"$model.modelList.targetGroup","as":"tg","cond":{"$eq":["$$tg.targetGroupId",ObjectId(targetGroupId)]}}},0]}}},
        { "$unwind": "$targetGroup.modelList" },
        { "$replaceRoot": { "newRoot":  "$targetGroup.modelList"} },
        { "$addFields": { "_key": { "$concat": ["$type", "_", { "$toString": "$retouching._id" } ] } } },
        { "$group": { "_id": "$_key", "type": { "$first": "$type"}, "retouching": { "$first": "$retouching"} , "models": { "$push": "$$ROOT" } } },
        { "$group": { "_id": "$type", "models": { "$push": { "retouching": "$retouching", "models": "$models" }  } } },
        { "$group": { "_id": null, "kv": { "$push": {"k": "$_id", "v": "$models" } } } },
        { "$project": { "models":{"$arrayToObject": "$kv"} } },
        { "$replaceRoot": { "newRoot": "$models" } }
    ])
        .toArray()
        .then(doc => doc && doc[0] ? doc[0] : {} );
}

async function getAllRetouchingForClient(clientProductId, targetGroupId) {
    let collectionConn = await connectAndGetCollection(dbName, clientProductMetadataCollection);
    return collectionConn.aggregate([
        { "$match": { "clientProductId": ObjectId(clientProductId), "keyName": "customRetouching" } },
        { "$project": { "targetGroup":{"$arrayElemAt":[{"$filter":{"input":"$properties.targetGroup","as":"tg","cond":{"$eq":["$$tg._id",ObjectId(targetGroupId)]}}},0]}}},
        { "$lookup":{"from":"defaultcatalogues","pipeline":[{"$match":{"$expr":{"$and":[{"$eq":["$catalogueType","retouching"]},{"$eq":["$productType","garment"]},{"$eq":["$type","default"]}]}}}],"as":"defaultRetouching"}},
        { "$addFields": { "defaultRetouching": {"$arrayElemAt": ["$defaultRetouching", 0] } } },
        { "$addFields": { "defaultRetouching":{"$arrayElemAt":[{"$filter":{"input": "$defaultRetouching.properties.targetGroup","as":"tg","cond":{"$eq":["$$tg._id",ObjectId(targetGroupId)]}}},0]}}},
        { "$project": { "retouchingList": { "$concatArrays": ["$defaultRetouching.values", "$targetGroup.retouchingList"] } } },
        { "$unwind": "$retouchingList" },
        { "$replaceRoot" : {"newRoot": "$retouchingList" } },
        { "$project": { "_id": "$_id", "image": "$image", "comments": "$comments" } },
        { "$group": { "_id": null, "kv": { "$push": { "k": { "$toString" : "$_id"}, "v": "$$ROOT" } } } },
        { "$replaceRoot" : { "newRoot": {"$arrayToObject":  "$kv"}  }  }
    ])
        .toArray()
        .then(doc => doc && doc[0] ? doc[0] : {} );
}

async function getPdfContentByType(type) {
    let collectionConn = await connectAndGetCollection(dbName, pdfMetadataCollection);
    return collectionConn.find({type}, { 'pdfContent': 1 })
        .toArray()
        .then((doc) => doc && doc[0] && doc[0].pdfContent);
}


async function savePdfUrl(type, pdfUrl, projectId, clientProductId, outputSpecId, destinations = []) {
    let collectionConn = await connectAndGetCollection(dbName, pdfStorageCollection);
    let resourceId = (getResourcesByType(type, projectId, clientProductId, outputSpecId));
    let resourceIdQueries = resourceId.map(el => { return { $elemMatch: { $eq: el } } });
    console.log('Saving Query ---> ', JSON.stringify({resourceId: { $all: resourceIdQueries }, type}))
    return collectionConn.findOneAndUpdate({resourceId: { $all: resourceIdQueries }, type}, { "$set": {
        type,
        resourceId,
        destinations,
        pdfUrl,
        updatedAt: new Date(),
        status: 30// "successful"
    } }, {upsert: false, new: false});
}

function addCustomKeys(outputSpec) {
    if (!outputSpec) return outputSpec;
    mergeProductRetouchingAndWrinkle(outputSpec);
    return outputSpec;
}

function getResourcesByType(pdfType, projectId, clientProductId, outputSpecId) {
    if (projectId) projectId = ObjectId(projectId);
    if (clientProductId) clientProductId = ObjectId(clientProductId);
    if (outputSpecId) outputSpecId = ObjectId(outputSpecId);
    
    switch (pdfType) {
        case PDF_TYPES.OUTPUT_SPECIFICATION:
            return [outputSpecId];
        case PDF_TYPES.MANNEQUIN:
            return [clientProductId];
        case PDF_TYPES.GENERIC:
            return [projectId, clientProductId, outputSpecId];
        case PDF_TYPES.TEMPLATE:
            return [projectId, clientProductId, outputSpecId];
        case PDF_TYPES.DRAPING:
            return [projectId, clientProductId, outputSpecId];
        case PDF_TYPES.GARMENT_RETOUCHING:
            return [projectId, clientProductId, outputSpecId];
    }
}

function mergeProductRetouchingAndWrinkle(outputSpec) {
    mergeOutputSpecFields(outputSpec, 'productRetouchingAndWrinkle', ["targetGroup", "0", "properties", "productRetouching"], ["outputSpec", "targetGroup", "0", "properties", "wrinkle"], ["targetGroup", "0", "properties", "wrinkle"]);
}

function mergeOutputSpecFields(outputSpec, keyName, path1, path2) {
    let key1 = extractSourceValueFromPath(outputSpec, path1);
    let key2 = extractSourceValueFromPath(outputSpec, path2);
    let newKey;
    if (!key1 && key2) {
        newKey = key2;
    } else if (key1 && !key2) {
        newKey = key1;
    } else if (key1 && key2) {
        newKey = [];
        let hash1 = {}, hash2 = {};
        for (let key of key1) hash1[key.category] = key.values;
        for (let key of key2) hash2[key.category] = key.values;

        // For adding keys in key2 and also common in key1
        for (let key of key2) {
            let key1Values = hash1[key.category] ? hash1[key.category] : [];
            newKey.push({
                _id: key._id,
                category: key.category,
                values: [
                    ...key1Values,
                    ...key.values 
                ]
            });
        }

        // For adding keys in key1 which were not present in key2
        for (let key of key1) {
            if (hash2[key.category]) continue;
            newKey.push({
                _id: key._id,
                category: key.category,
                values: [
                    ...key.values 
                ]
            });
        }
    } else {
        newKey = null;
    }
    if (!outputSpec['_customFields']) outputSpec['_customFields'] = {};
    outputSpec['_customFields'][keyName] = newKey && newKey.length ? newKey : null ;
}

function extractSourceValueFromPath(source, path) {
    let value = source;
    for (let key of path) {
        if (!value) return;
        value = value[key];
    }
    return value;
}
