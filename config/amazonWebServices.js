const AWS = require('aws-sdk');
const { AWS_ACCESS_KEY, AWS_REGION, AWS_SECRET_KEY } = require('./config');

AWS.config.update({accessKeyId: AWS_ACCESS_KEY, secretAccessKey: AWS_SECRET_KEY ,signatureVersion: 'v4',region: AWS_REGION});

module.exports = AWS;